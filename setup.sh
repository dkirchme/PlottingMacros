#!/bin/bash

################################################################################
# setup for plotting macros
################################################################################
echo "Setup ATLAS"
setupATLAS
asetup 21.2.83,AnalysisBase

# Start
echo "Setup plotting macros..."

if [ $PLOTTING_MACRO_ROOTDIR ]; then
	echo "PLOTTING_MACRO_ROOTDIR is already defined, some configuration maybe overwritten!"
fi

# Setup Paths
export PLOTTING_MACRO_ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Setup PYTHONPATH variable
export PYTHONPATH=${PLOTTING_MACRO_ROOTDIR}/include:${PYTHONPATH}

