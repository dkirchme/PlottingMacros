#!/usr/bin/python

import ROOT
import os
import sys
from DKColor import getWhiteToBlue

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# file = "/ZIH.fast/users/kirchmeier/Run/run10/data-DTNT/user.dkirchme.mc15_13TeV.303366.RS_G_hh_bbtt_hh_c10_M1800.recon.AOD.e4438_s2608_r6869_v02.root"
file = "/ZIH.fast/users/kirchmeier/Run/withDTESrun10_E/data-DTNT/user.dkirchme.mc15_13TeV.303366.RS_G_hh_bbtt_hh_c10_M1800.recon.AOD.e4438_s2608_r6869_v02.root"

f = ROOT.TFile(file)
t = f.Get("tree")

ROOT.gStyle.SetPaintTextFormat(".3f")
n_colors, palette = getWhiteToBlue()
ROOT.gStyle.SetPalette(n_colors, palette)


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


c = ROOT.TCanvas()
t.Draw("truth_n_tracks_subl:n_tracks_subl>>hreco(6,0,6,5,0,5)", "truth_matched")
t.Draw("truth_n_tracks_lead:n_tracks_lead>>+hreco", "truth_matched")


t.Draw("truth_n_tracks_subl>>htruth1", "truth_n_tracks_subl==1")
t.Draw("truth_n_tracks_lead>>+htruth1", "truth_n_tracks_lead==1")
t.Draw("truth_n_tracks_subl>>htruth3", "truth_n_tracks_subl==3")
t.Draw("truth_n_tracks_lead>>+htruth3", "truth_n_tracks_lead==3")

ntruth1 = ROOT.gDirectory.Get("htruth1").GetEntries()
ntruth3 = ROOT.gDirectory.Get("htruth3").GetEntries()

hreco = ROOT.gDirectory.Get("hreco")

h = ROOT.TH2F("h", "", 2, 0, 1, 2, 0, 1)
xaxis = h.GetXaxis()
yaxis = h.GetYaxis()
xaxis.SetBinLabel(1, "reco 1p")
xaxis.SetBinLabel(2, "reco mp")
yaxis.SetBinLabel(1, "true 1p")
yaxis.SetBinLabel(2, "true mp")

n = ntruth1+ntruth3
h.SetBinContent(1, 1, hreco.Integral(2,2, 2,2) / n )  # true 1p reco 1p
h.SetBinContent(2, 1, hreco.Integral(3,4, 2,2) / n )  # true 1p reco mp
h.SetBinContent(1, 2, hreco.Integral(2,2, 4,4) / n )  # true mp reco 1p
h.SetBinContent(2, 2, hreco.Integral(3,4, 4,4) / n )  # true mp reco mp

h.Draw("coltext")
c.Update()
c.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_fractions.png")

# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------




ce = ROOT.TCanvas()
t.Draw("truth_n_tracks_subl:n_tracks_subl>>hpas(6,0,6,5,0,5)", "truth_matched")
t.Draw("truth_n_tracks_lead:n_tracks_lead>>+hpas", "truth_matched")

t.Draw("truth_n_tracks_subl:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
for i in range(10):
   t.Draw("truth_n_tracks_subl:{}>>+htot".format(i), "truth_matched", "COLTEXT")
   t.Draw("truth_n_tracks_lead:{}>>+htot".format(i), "truth_matched", "COLTEXT")

hpas = ROOT.gDirectory.Get("hpas")
htot = ROOT.gDirectory.Get("htot")

hpas.Divide(htot)
hpas.GetYaxis().SetTitle("true n_{tracks}")
hpas.GetXaxis().SetTitle("reco n_{tracks}")

hpas.Draw("COLTEXT")
ce.Update()
ce.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency.png")

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


c2 = ROOT.TCanvas()
t.Draw("truth_n_tracks_subl:n_tracks_subl>>hpas(6,0,6,5,0,5)", "truth_matched")

t.Draw("truth_n_tracks_subl:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
for i in range(10):
   t.Draw("truth_n_tracks_subl:{}>>+htot".format(i), "truth_matched", "COLTEXT")

hpas = ROOT.gDirectory.Get("hpas")
htot = ROOT.gDirectory.Get("htot")

hpas.Divide(htot)
hpas.GetYaxis().SetTitle("true n_{tracks}^{subleading}")
hpas.GetXaxis().SetTitle("reco n_{tracks}^{subleading}")

hpas.Draw("COLTEXT")
c2.Update()
c2.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency_subl.png")


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


c3 = ROOT.TCanvas()
t.Draw("truth_n_tracks_lead:n_tracks_lead>>hpas(6,0,6,5,0,5)", "truth_matched")

t.Draw("truth_n_tracks_lead:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
for i in range(10):
   t.Draw("truth_n_tracks_lead:{}>>+htot".format(i), "truth_matched", "COLTEXT")

hpas = ROOT.gDirectory.Get("hpas")
htot = ROOT.gDirectory.Get("htot")

hpas.Divide(htot)
hpas.GetYaxis().SetTitle("true n_{tracks}^{leading}")
hpas.GetXaxis().SetTitle("reco n_{tracks}^{leading}")

hpas.Draw("COLTEXT")
c3.Update()
c3.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency_lead.png")










# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------
# c_subl = ROOT.TCanvas()
# t.Draw("truth_n_tracks_subl:n_tracks_subl>>hpas_subl(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# t.Draw("truth_n_tracks_subl:-1>>htot_subl(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# for i in range(10):
#    t.Draw("truth_n_tracks_subl:{}>>+htot_subl".format(i), "truth_matched", "COLTEXT")

# hpas_subl = ROOT.gDirectory.Get("hpas_subl")
# htot_subl = ROOT.gDirectory.Get("htot_subl")

# hpas_subl.Divide(htot_subl)
# hpas_subl.GetYaxis().SetTitle("truth n_{tracks}^{sublead}")
# hpas_subl.GetXaxis().SetTitle("reco n_{tracks}^{sublead}")

# hpas_subl.Draw("COLTEXT")
# c_subl.Update()


# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------
# c_lead = ROOT.TCanvas()
# t.Draw("truth_n_tracks_lead:n_tracks_lead>>hpas_lead(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# t.Draw("truth_n_tracks_lead:-1>>htot_lead(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# for i in range(10):
#    t.Draw("truth_n_tracks_lead:{}>>+htot_lead".format(i), "truth_matched", "COLTEXT")

# hpas_lead = ROOT.gDirectory.Get("hpas_lead")
# htot_lead = ROOT.gDirectory.Get("htot_lead")

# hpas_lead.Divide(htot_lead)
# hpas_lead.GetYaxis().SetTitle("truth n_{tracks}^{lead}")
# hpas_lead.GetXaxis().SetTitle("reco n_{tracks}^{lead}")

# hpas_lead.Draw("COLTEXT")
# c_lead.Update()

