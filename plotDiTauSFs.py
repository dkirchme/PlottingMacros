#!/usr/bin/python

import ROOT
import os
import sys
from math import sqrt
from array import array
from DKColor import DKRainbow
from DKColor import DKColor
import pdb

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# file = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv49/Merged.root"
# file = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv52/Merged.root"
# file = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv56/Merged.root"
file = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_truthditau_Rv64/Merged.root"

f = ROOT.TFile(file)

signal = "TruthMatchedDiTau"

# bkg_samples = ["stopWt", "stops", "stopt", "WWPw", "WZPw", "ZZPw", "Wl", "Wcl", "Wcc", "Wbl", "Wbc", "Wbb", "ttbar_nonallhad", "ttbar_allhad", "fakes", "Zee_Sh221", "Zl", "Zcl", "Zcc", "Zbl", "Zbc", "Zbb"]
# bkg_samples = ["stopWt", "stopt", "WWPw", "WZPw", "ZZPw", "Wl", "Wcl", "Wcc", "Wbl", "Wbc", "Wbb", "ttbar_nonallhad", "ttbar_allhad", "fakes", "Zee_Sh221", "Zl", "Zcl", "Zcc", "Zbl", "Zbc", "Zbb"]
bkg_samples = ["stopWt", "WWPw", "WZPw", "ZZPw", "Wl", "Wcl", "Wcc", "Wbl", "Wbc", "Wbb", "ttbar_nonallhad", "ttbar_allhad", "fakes", "Zee_Sh221", "Zl", "Zcl", "Zcc", "Zbl", "Zbc", "Zbb"]

data = "data"

region = "0tag2pjet_0ptv_SR_SelDTsubsM"

hmap = {}

name  = "{}_{}".format(signal, region)
h_sig = f.Get(name)
hmap[signal] = h_sig
hmap[signal].GetXaxis().SetRangeUser(0, 300)
# hmap[s].Rebin(4)

hmap["bkg"] = hmap[signal].Clone()
hmap["bkg"].Reset()

for b in bkg_samples: 
    # print b
    name  = "{}_{}".format(b, region)
    h_bkg = f.Get(name)
    hmap[b] = h_bkg
    if h_bkg == None: 
        print "WARNING skip", b
        continue
    hmap[b].GetXaxis().SetRangeUser(0, 300)
    # hmap[b].Rebin(4)
    hmap["bkg"].Add(h_bkg)


name  = "{}_{}".format(data, region)
h_data = f.Get(name)
hmap[data] = h_data
hmap[data].GetXaxis().SetRangeUser(0, 300)
# hmap[data].Rebin(4)

    


# c = []
# for r in regions:
#     c.append(ROOT.TCanvas())
#     c[-1].cd()
#     hmap[r+"pas"].GetYaxis().SetTitle("S / sqrt(B)")
#     hmap[r+"pas"].SetMarkerColor(ROOT.kBlue)
#     hmap[r+"pas"].SetLineColor(ROOT.ROOT.kBlue)
#     hmap[r+"pas"].GetXaxis().SetRangeUser(0, 300)
#     hmap[r+"pas"].Rebin(4)
#     hmap[r+"tot"].Draw("PL")
#     hmap[r+"pas"].Draw("SAMEPL")

#     c[-1].SaveAs("output/vetoStudies/significance_{}.pdf".format(r))


colors = {
    "stopWt":             ROOT.kOrange - 6 ,
    # "stops":              ROOT.kOrange - 6 ,
    # "stopt":              ROOT.kOrange - 6 ,
    "WWPw":               ROOT.kGray + 1 ,
    "WZPw":               ROOT.kGray + 1 ,
    "ZZPw":               ROOT.kGray + 1 ,
    "Wl":                 ROOT.kBlue - 10 ,
    "Wcl":                ROOT.kBlue - 10 ,
    "Wcc":                ROOT.kBlue - 10 ,
    "Wbl":                ROOT.kBlue - 10 ,
    "Wbc":                ROOT.kBlue - 10 ,
    "Wbb":                ROOT.kBlue - 10 ,
    "ttbar_nonallhad":    ROOT.kOrange ,
    "ttbar_allhad":       ROOT.kOrange-1 ,
    "fakes":              ROOT.kMagenta - 9 ,
    "Zee_Sh221":          ROOT.kCyan + 1 ,
    "Zl":                 ROOT.kAzure - 9 ,
    "Zcl":                ROOT.kAzure - 8 ,
    "Zcc":                ROOT.kAzure - 4 ,
    "Zbl":                ROOT.kAzure + 1 ,
    "Zbc":                ROOT.kAzure + 2 ,
    "Zbb":                ROOT.kAzure + 3 ,
    "TruthMatchedDiTau":  ROOT.kViolet ,
    "bkg": ROOT.kGray,
}



for k, v in colors.iteritems():
    hmap[k].SetFillColor(v)
    hmap[k].SetLineColor(v)



# if(hmap["stopWt_"+r]):
#     hmap["stopWt_"+r].SetFillColor( ROOT.kOrange - 6)
#     hmap["stopWt_"+r].SetLineColor( ROOT.kOrange - 6)
# if(hmap["stops_"+r]):
#     hmap["stops_"+r].SetFillColor( ROOT.kOrange - 6)
#     hmap["stops_"+r].SetLineColor( ROOT.kOrange - 6)
# if(hmap["stopt_"+r]):
#     hmap["stopt_"+r].SetFillColor( ROOT.kOrange - 6)
#     hmap["stopt_"+r].SetLineColor( ROOT.kOrange - 6)
# if(hmap["WWPw_"+r]):
#     hmap["WWPw_"+r].SetFillColor(ROOT.kGray + 1)
#     hmap["WWPw_"+r].SetLineColor(ROOT.kGray + 1)
# if(hmap["WZPw_"+r]):
#     hmap["WZPw_"+r].SetFillColor(ROOT.kGray + 1)
#     hmap["WZPw_"+r].SetLineColor(ROOT.kGray + 1)
# if(hmap["ZZPw_"+r]):
#     hmap["ZZPw_"+r].SetFillColor(ROOT.kGray + 1)
#     hmap["ZZPw_"+r].SetLineColor(ROOT.kGray + 1)
# if(hmap["Wl_"+r]):
#     hmap["Wl_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wl_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["Wcl_"+r]):
#     hmap["Wcl_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wcl_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["Wcc_"+r]):
#     hmap["Wcc_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wcc_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["Wbl_"+r]):
#     hmap["Wbl_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wbl_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["Wbc_"+r]):
#     hmap["Wbc_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wbc_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["Wbb_"+r]):
#     hmap["Wbb_"+r].SetFillColor( ROOT.kBlue - 10)
#     hmap["Wbb_"+r].SetLineColor( ROOT.kBlue - 10)
# if(hmap["ttbar_nonallhad_"+r]):
#     hmap["ttbar_nonallhad_"+r].SetFillColor( ROOT.kOrange)
#     hmap["ttbar_nonallhad_"+r].SetLineColor( ROOT.kOrange)
# if(hmap["ttbar_allhad_"+r]):
#     hmap["ttbar_allhad_"+r].SetFillColor( ROOT.kOrange-1)
#     hmap["ttbar_allhad_"+r].SetLineColor( ROOT.kOrange-1)
# if(hmap["fakes_"+r]):
#     hmap["fakes_"+r].SetFillColor(ROOT.kMagenta - 9)
#     hmap["fakes_"+r].SetLineColor(ROOT.kMagenta - 9)
# if(hmap["Zee_Sh221_"+r]):
#     hmap["Zee_Sh221_"+r].SetFillColor(ROOT.kCyan + 1)
#     hmap["Zee_Sh221_"+r].SetLineColor(ROOT.kCyan + 1)
# if(hmap["Zl_"+r]):
#     hmap["Zl_"+r].SetFillColor( ROOT.kAzure - 9)
#     hmap["Zl_"+r].SetLineColor( ROOT.kAzure - 9)
# if(hmap["Zcl_"+r]):
#     hmap["Zcl_"+r].SetFillColor( ROOT.kAzure - 8)
#     hmap["Zcl_"+r].SetLineColor( ROOT.kAzure - 8)
# if(hmap["Zcc_"+r]):
#     hmap["Zcc_"+r].SetFillColor( ROOT.kAzure - 4)
#     hmap["Zcc_"+r].SetLineColor( ROOT.kAzure - 4)
# if(hmap["Zbl_"+r]):
#     hmap["Zbl_"+r].SetFillColor( ROOT.kAzure + 1)
#     hmap["Zbl_"+r].SetLineColor( ROOT.kAzure + 1)
# if(hmap["Zbc_"+r]):
#     hmap["Zbc_"+r].SetFillColor( ROOT.kAzure + 2)
#     hmap["Zbc_"+r].SetLineColor( ROOT.kAzure + 2)
# if(hmap["Zbb_"+r]):
#     hmap["Zbb_"+r].SetFillColor( ROOT.kAzure + 3)
#     hmap["Zbb_"+r].SetLineColor( ROOT.kAzure + 3)
# if(hmap["TruthMatchedDiTau_"+r]):
#     hmap["TruthMatchedDiTau_"+r].SetFillColor( ROOT.kViolet)
#     hmap["TruthMatchedDiTau_"+r].SetLineColor( ROOT.kViolet)



c = []
hs = ROOT.THStack("hs", "")
hs2 = ROOT.THStack("hs2", "")

hs2.Add(hmap["bkg"])

for b in bkg_samples: 
    if (hmap[b]): 
        hs.Add(hmap[b])

hs.Add(hmap[signal])
hs2.Add(hmap[signal])


c.append(ROOT.TCanvas())
c[-1].cd()
hs.Draw("hist")
hs.GetXaxis().SetRangeUser(0, 300)
hs.SetMaximum(92)
hs.Draw("hist")
hmap[data].Draw("SAME")



c.append(ROOT.TCanvas())
c[-1].cd()
hs2.Draw("hist")
hs2.GetXaxis().SetRangeUser(0, 300)
hs2.SetMaximum(92)
hs2.Draw("hist")
hmap[data].Draw("SAME")

##########################################################
# mass window cuts
##########################################################
mwindows = []
for i in range(6, 26):
    for j in range(i, 26):
        S = hmap[signal].Integral(i, j)
        B = hmap["bkg"].Integral(i, j)
        D = hmap[data].Integral(i, j)
        mwindows.append({"lower": i, "upper": j, "lowerU": hmap[signal].GetBinLowEdge(i), "upperU": hmap[signal].GetBinLowEdge(j+1),   "S": S, "B": B, "D": D,  "SSB": S/(S+B), "SrB": S/sqrt(B), "SrSB": S/sqrt(S+B) })

maxSSB = max(mwindows, key=lambda t: t["SSB"])
print "SSB  l:", maxSSB["lowerU"], " u:", maxSSB["upperU"], "lbin:", maxSSB["lower"], "ubin:", maxSSB["upper"]
maxSrB = max(mwindows, key=lambda t: t["SrB"])
print "SrB  l:", maxSrB["lowerU"], " u:", maxSrB["upperU"], "lbin:", maxSrB["lower"], "ubin:", maxSrB["upper"]
maxSrSB = max(mwindows, key=lambda t: t["SrSB"])
print "SrSB l:", maxSrSB["lowerU"], " u:", maxSrSB["upperU"], "lbin:", maxSrSB["lower"], "ubin:", maxSrSB["upper"]

# SSB  l: 65.0  u: 70.0 lbin: 14 ubin: 14
# SrB  l: 35.0  u: 85.0 lbin: 8 ubin: 17
# SrSB l: 30.0  u: 90.0 lbin: 7 ubin: 18 <-----------



##########################################################
# SF calculation
##########################################################
print "S+B:", maxSrSB["S"]+maxSrSB["B"], "DATA:", maxSrSB["D"]
print "DATA - B:", maxSrSB["D"] - maxSrSB["B"], "S:", maxSrSB["S"]
print "(DATA-B) / S :", (maxSrSB["D"] - maxSrSB["B"]) / maxSrSB["S"]

# S+B: 540.088470221 DATA: 453.0
# DATA - B: 323.442517996 S: 410.530988216
# (DATA-B) / S : 0.787863833132    <----------------


# >>> print "S+B:", maxSrB["S"]+maxSrB["B"], "DATA:", maxSrB["D"]
# >>> print "DATA - B:", maxSrB["D"] - maxSrB["B"], "S:", maxSrB["S"]
# >>> print "(DATA-B) / S :", (maxSrB["D"] - maxSrB["B"]) / maxSrB["S"]
# S+B: 511.127541304 DATA: 428.0
# DATA - B: 311.054367304 S: 394.181908607
# (DATA-B) / S : 0.78911375817

# >>> maxS = max(mwindows, key=lambda t: t["S"])
# >>> print "S+B:", maxS["S"]+maxS["B"], "DATA:", maxS["D"]
# >>> print "DATA - B:", maxS["D"] - maxS["B"], "S:", maxS["S"]
# >>> print "(DATA-B) / S :", (maxS["D"] - maxS["B"]) / maxS["S"]
# S+B: 605.308603204 DATA: 514.0
# DATA - B: 334.906816199 S: 426.215419403
# (DATA-B) / S : 0.785768888109


# SF = (maxSrSB["D"] - maxSrSB["B"]) / maxSrSB["S"]


# READER VERSION 52:
# SSB  l: 65.0  u: 70.0 lbin: 14 ubin: 14
# SrB  l: 35.0  u: 85.0 lbin: 8 ubin: 17
# SrSB l: 30.0  u: 95.0 lbin: 7 ubin: 19 <---------------
# S+B: 362.292627752 DATA: 315.0
# DATA - B: 222.110717595 S: 269.403345346
# (DATA-B) / S : 0.824454192686
# SF: (D-B)/S = 0.802598379863
# SF= 0.802598379863  +- 0.0673050435037 (stat)


# ##########################################################
# # post SF plots
# ##########################################################
# hs3 = ROOT.THStack("hs3", "")
# for b in bkg_samples: 
#     if (hmap[b]): 
#         hs3.Add(hmap[b])

# hmap[signal+"SF"] = hmap[signal].Clone()
# hmap[signal+"SF"].Scale(SF)

# hs3.Add(hmap[signal+"SF"])
# c.append(ROOT.TCanvas())
# c[-1].cd()
# hs3.Draw("hist")
# hs3.GetXaxis().SetRangeUser(0, 300)
# hs3.SetMaximum(92)
# hs3.Draw("hist")
# hmap[data].Draw("SAME")


##########################################################
# rebinning (use only mass window)
##########################################################

x_bins = array('d', [0, 30, 90, 300])

for b in bkg_samples:
    hmap[b+'rebin'] = hmap[b].Rebin(len(x_bins)-1, b+"rebin", x_bins)
    for i in range(len(x_bins)+1):
        if (i == 2): continue  # bin that contains the mass window
        hmap[b+'rebin'].SetBinContent(i, 0)
        hmap[b+'rebin'].SetBinError(i, 0)

hmap[signal+'rebin'] = hmap[signal].Rebin(len(x_bins)-1, signal+"rebin", x_bins)
hmap[data+'rebin'] = hmap[data].Rebin(len(x_bins)-1, data+"rebin", x_bins)
hmap['bkg'+'rebin'] = hmap['bkg'].Rebin(len(x_bins)-1, 'bkg'+"rebin", x_bins)

for i in range(len(x_bins)+1):
    if (i == 2): continue  # bin that contains the mass window
    hmap[signal+'rebin'].SetBinContent(i, 0)
    hmap[signal+'rebin'].SetBinError(i, 0)
    hmap[data+'rebin'].SetBinContent(i, 0)
    hmap[data+'rebin'].SetBinError(i, 0)


hs4 = ROOT.THStack("hs4", "")
for b in bkg_samples: 
    if (hmap[b+'rebin']): 
        hs4.Add(hmap[b+'rebin'])

hs4.Add(hmap[signal+"rebin"])
c.append(ROOT.TCanvas())
c[-1].cd()
hs4.Draw("hist")
hs4.GetXaxis().SetRangeUser(0, 300)
hs4.SetMaximum(600)
hs4.Draw("hist")
hmap[data+'rebin'].Draw("SAME")

##########################################################
# stat uncertainties
##########################################################

D = hmap[data+'rebin'].GetBinContent(2)
B = hmap['bkg'+'rebin'].GetBinContent(2)
S = hmap[signal+'rebin'].GetBinContent(2)
dD = hmap[data+'rebin'].GetBinError(2)
dB = hmap['bkg'+'rebin'].GetBinError(2)
dS = hmap[signal+'rebin'].GetBinError(2)

SF = (D-B)/S

print 'SF: (D-B)/S =', SF
d_stat_SF = sqrt((dD/S)**2 + (dB/S)**2 + ( (D-B)*dS/S**2 )**2)

print 'SF=', SF, " +-", d_stat_SF, "(stat)"



##########################################################
# syst uncertainties
##########################################################

d_sys_up = []
d_sys_dw = []


systematics = ["SysFF_Stat", "SysFF_Transition_Btag", "SysFF_Transition_Sign", "SysFATJET_Medium_JET_Comb_Baseline_Kin", "SysFATJET_Medium_JET_Comb_Modelling_Kin", "SysFATJET_Medium_JET_Comb_TotalStat_Kin", "SysFATJET_Medium_JET_Comb_Tracking_Kin", "SysFT_EFF_Eigen_B_0_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_B_1_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_B_2_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_C_0_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_C_1_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_C_2_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_C_3_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_Light_0_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_Light_1_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_Light_2_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_Eigen_Light_3_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_extrapolation_AntiKtVR30Rmax4Rmin02TrackJets", "SysFT_EFF_extrapolation_from_charm_AntiKtVR30Rmax4Rmin02TrackJets", "SysMET_SoftTrk_Scale", "SysTAUS_TRUEHADDITAU_EFF_JETID_TOTAL", "SysTAUS_TRUEHADDITAU_SME_TES_TOTAL", "SysFATJET_JER", "SysFATJET_JMR", "SysMET_SoftTrk_ResoPara", "SysMET_SoftTrk_ResoPerp"]
# systematics = ["SysFF_Stat", "SysFF_Transition_Btag", "SysFATJET_Medium_JET_Comb_Baseline_Kin", "SysFATJET_Medium_JET_Comb_Modelling_Kin", "SysFATJET_Medium_JET_Comb_Tracking_Kin"]


for sys in systematics:
    print "Run systematic", sys
    for direction in ["up", "down"]:
        h_sig = f.Get("Systematics/{}_{}_{}__1{}".format(signal, region, sys, direction))
        if (not h_sig): continue
        h_sig = h_sig.Rebin(len(x_bins)-1, signal+sys+direction+"rebin", x_bins)

        h_bkg = h_sig.Clone()
        h_bkg.Reset()
        for b in bkg_samples: 
            h_bkg.Add(f.Get("Systematics/{}_{}_{}__1{}".format(b, region, sys, direction)).Rebin(len(x_bins)-1, b+sys+"rebin", x_bins))


        sysB = h_bkg.GetBinContent(2)
        sysS = h_sig.GetBinContent(2)
        sysSF = (D-sysB)/sysS

        dsysB = abs(B-sysB)
        dsysS = abs(S-sysS)
        is_up_systematic = (SF-sysSF) < 0
        if is_up_systematic:
            d_sys_up.append((sys+direction, dsysS, dsysB))
        else:
            d_sys_dw.append((sys+direction, dsysS, dsysB))

        # print sys, direction
        # print "sysSF:", sysSF
        # print "sysB:", sysB
        # print "sysS:", sysS
        # pdb.set_trace()



dSF2_up = 0
dSF2_dw = 0

for sys in d_sys_up:
    dSF2_up += ((D-B)/(S**2) * sys[1])**2 + (1/S * sys[2])**2 # dS = sys[1], dB = sys[2]

for sys in d_sys_dw:
    dSF2_dw += ((D-B)/(S**2) * sys[1])**2 + (1/S * sys[2])**2 # dS = sys[1], dB = sys[2]


dSF_up = sqrt(dSF2_up)
dSF_dw = sqrt(dSF2_dw)

print 'SF=', SF, " +-", d_stat_SF, "(stat) +", dSF_up, "-", dSF_dw, "(syst)"
print "stat. data: ", (dD/S) 
print "stat. MC", sqrt((dB/S)**2 + ( (D-B)*dS/S**2 )**2)
# Rv49: 
# SF= 0.787863863067  +- 0.0535885143804 (stat) + 0.194150653405 - 0.205065904754 (syst)

# Rv52: 
# SF= 0.802598379863  +- 0.0673050435037 (stat) + 0.186939861117 - 0.205968439359 (syst)

# Rv56:
# SF= 0.818878574694  +- 0.068666758695 (stat) + 0.188278274418 - 0.209525074936 (syst)


print "D, S, B:", D, S, B
# 298.0 259.788696289 85.2646026611


# Rv56 output:
# SSB  l: 65.0  u: 70.0 lbin: 14 ubin: 14
# SrB  l: 35.0  u: 85.0 lbin: 8 ubin: 17
# SrSB l: 30.0  u: 95.0 lbin: 7 ubin: 19
# S+B: 354.636812389 DATA: 313.0
# DATA - B: 221.769292176 S: 263.406104565
# (DATA-B) / S : 0.841929204877
# SF: (D-B)/S = 0.818878574694
# SF= 0.818878574694  +- 0.068666758695 (stat)
# Run systematic SysFF_Stat
# Run systematic SysFF_Transition_Btag
# Run systematic SysFF_Transition_Sign
# Run systematic SysFATJET_Medium_JET_Comb_Baseline_Kin
# Run systematic SysFATJET_Medium_JET_Comb_Modelling_Kin
# Run systematic SysFATJET_Medium_JET_Comb_TotalStat_Kin
# Run systematic SysFATJET_Medium_JET_Comb_Tracking_Kin
# Run systematic SysFT_EFF_Eigen_B_0_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_B_1_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_B_2_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_C_0_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_C_1_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_C_2_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_C_3_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_Light_0_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_Light_1_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_Light_2_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_Eigen_Light_3_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_extrapolation_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysFT_EFF_extrapolation_from_charm_AntiKtVR30Rmax4Rmin02TrackJets
# Run systematic SysMET_SoftTrk_Scale
# Run systematic SysTAUS_TRUEHADDITAU_EFF_JETID_TOTAL
# Run systematic SysTAUS_TRUEHADDITAU_SME_TES_TOTAL
# Run systematic SysFATJET_JER
# Run systematic SysFATJET_JMR
# Run systematic SysMET_SoftTrk_ResoPara
# Run systematic SysMET_SoftTrk_ResoPerp
# SF= 0.818878574694  +- 0.068666758695 (stat) + 0.188278274418 - 0.209525074936 (syst)
# D, S, B: 298.0 259.788696289 85.2646026611



# Rv64:
# SF= 0.864016184634  +- 0.0877313132185 (stat) + 0.205220092566 - 0.218192557524 (syst)
# D, S, B: 201.0 167.025527954 56.6872406006
