################################################################################
# * holding plot informations
# * the main object is a plot which has:
#   * a name
#   * a canvas where the plot is drawn in
#   * a dict of graphs.
# * The GraphHolder has:
#   * a name
#   * a graph type (e.g. efficiency plot)
#   * a dict of paths. A path points to the input histogram inside the root file
#   * a graph. This can be a histogram, a function etc.
################################################################################

# python includes
from collections import OrderedDict

# common includes
from MacroLogger import BaseLoggerClass
from OptionHandler import BaseOptionClass

from pdb import set_trace as bp


################################################################################
class PlotHolder(BaseLoggerClass, BaseOptionClass):
    """docstring for PlotHolder"""
    #_______________________________________
    def __init__(self, name, logLevel="WARNING"):

        BaseLoggerClass.__init__(self, name, logLevel)
        BaseOptionClass.__init__(self, name, logLevel)

        self.name = str(name)           # name of the plot
        self.type = ''             # type of the plot, e.g. SimpleHist
        self.canvas = None         # canvas to save results
        self.graphHolders = OrderedDict()  # dict of graphs. Key: name, Value: graph

        self.name = self._ReplaceImproperChars(self.name)

    #_______________________________________
    def AddGraphHolder(self, graph):
        name = graph.name
        if name in self.graphHolders:
            self.logger.warning("Graph {} already known, overwrite!".format(name))

        self.graphHolders[name] = graph

    #_______________________________________
    def Print(self):
        print "Graphs stored in this Plot:"
        for key in self.graphHolders.keys():
            print "\t- {}".format(key)

    #_______________________________________
    def GetGraphHolders(self):
        return self.graphHolders

    #_______________________________________
    def GetGraphHolder(self, name):
        if name in self.graphHolders:
            return self.graphHolders[name]
        else:
            self.logger.warning('GraphHolder with name {} \
                is not known. Return empty.'.format(name))
            return None

    #_______________________________________
    def GetGraph(self, name):
        if name in self.graphHolders:
            return self.graphHolders[name].graph
        else:
            self.logger.warning("Graph name {} is not known. Return None.".format(name))
            return None

    #_______________________________________
    def GetGraphSystematics(self, name, systematic):
        if not name in self.graphHolders:
            self.logger.warning("Graph name {} is not known. Return None.".format(name))
            return None

        if not systematic in self.graphHolders[name].sysgraphs:
            self.logger.warning("Systematic name {} is not known. Return None.".format(systematic))
            return None

        return self.graphHolders[name].sysgraphs[systematic]

    #_______________________________________
    def GetGraphNames(self):
        return self.graphHolders.keys()

    # #_______________________________________
    # def GetGraphSystematicsNames(self, name):
    #     if not name in self.graphHolders:
    #         self.logger.warning("Graph name {} is not known. Return None.".format(name))
    #         return None

    #     return self.graphHolders[name].systematicslist


    #_______________________________________
    def IsValid(self):
        return len(self.graphHolders) > 0

    #_______________________________________
    def _ReplaceImproperChars(self, string):
        string = string.replace('/', '.')
        string = string.replace('(', '_')
        string = string.replace(')', '_')

        return string


################################################################################
class GraphHolder(BaseOptionClass):
    #_______________________________________
    def __init__(self, name, graphType, logLevel="WARNING"):
        BaseOptionClass.__init__(self, name, logLevel)

        self.name = str(name)
        self.type = graphType  # graph type. e.g. efficiency plot
        self.path = OrderedDict()     # path to input hist in root file
        self.graph = None      # the graph. e.g. TH1F
        self.systematicslist = []      # list of systematics
        self.sysgraphs = OrderedDict() # dict of systematics : {sys_name: graph}

    #_______________________________________
    def SetGraph(self, graph):
        self.graph = graph

    #_______________________________________
    def SetGraphType(self, graphType):
        self.type = graphType

    #_______________________________________
    def GetGraphType(self):
        return self.type

    #_______________________________________
    def SetSystematics(self, systematics):
        self.systematicslist = systematics

    #_______________________________________
    def GetSystematics(self):
        return self.systematicslist
