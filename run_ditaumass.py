import ROOT

# input_file_path = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv63/Merged.root"
input_file_path = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv66/Merged.root"

f = ROOT.TFile(input_file_path, "READ")



def print_bkg_eff(signal, region):
    # signal = "Xtohh2500_Hw"
    h = f.Get(signal+region)
    h.Rebin(5)

    nbins = h.GetNbinsX()
    icut = 0
    for i in range(1, nbins+1):
        eff = h.Integral(i, nbins+1) / h.Integral()
        if eff < 0.99: 
            # print i
            break
        icut = i



    bkgs = ["fakes", "Wbb", "Zbb", "Zbc", "Zbl", "Zcc", "Zcl", "Zl", "ZZPw"]

    n_bkg_tot = 0
    n_bkg_cut = 0
    for b in bkgs: 
        hb = f.Get(b+region)
        if hb.GetNbinsX() == 400:
            hb.Rebin(5)
        n_bkg_tot += hb.Integral()
        n_bkg_cut += hb.Integral(icut, nbins+1)

    # print "==========================================="
    print "signal: ", signal
    # print "bin:", icut, "... m>", h.GetBinLowEdge(icut), "... eff:", h.Integral(icut, nbins+1) / h.Integral()
    # print "total background:", n_bkg_tot, "after cut:", n_bkg_cut, "bkg eff:", n_bkg_cut/n_bkg_tot*100, "%"
    print "cut:", h.GetBinLowEdge(icut), "signal:", h.Integral(icut, nbins+1) / h.Integral() *100, "bkg:", n_bkg_cut/n_bkg_tot*100

    # print "==========================================="



masses = ["subsmhh", "effmHH", "constrVismHH", "constrEffmHH", "collmHH"]
signals = ["Xtohh1000_Hw", "Xtohh1200_Hw", "Xtohh1400_Hw", "Xtohh1600_Hw", "Xtohh1800_Hw", "Xtohh2000_Hw", "Xtohh2500_Hw", "Xtohh3000_Hw"]

for mass in masses:

    print "MASS:", mass
    region = "_2tag2pjet_0ptv_MwindowDphiLt1_"+mass

    for s in signals: 
        print_bkg_eff(s, region)
    print "=============================================="


