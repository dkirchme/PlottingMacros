#!/usr/bin/python

import ROOT
import os
import sys
from math import sqrt
# from DKColor import getWhiteToBlue

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# productions = ["submitDir_v9_Rv38_fixedLeadJetPtCut"]
productions = ["submitDir_v9_Rv38_fixedLeadJetPtCut", "submitDir_v9_Rv38"]
# productions = ["submitDir_v9_Rv38"]

signal_masses = [1000, 1200, 1400, 1600, 1800, 2000, 2500, 3000]
signal_samples = ["Xtohh{}_Hw".format(m) for m in signal_masses]

bkg_samples = ["stopWt", "stops", "stopt", "WWPw", "WZPw", "ZZPw", "Wl", "Wcl", "Wcc", "Wbl", "Wbc", "Wbb", "ttbar_nonallhad", "ttbar_allhad", "fakes", "Zee_Sh221", "Zl", "Zcl", "Zcc", "Zbl", "Zbc", "Zbb"]


regions = ["2tag2pjet_0ptv_SR_SelDTPhi", "1tag2pjet_0ptv_SR_SelDTPhi"]
hmap = {}
f = []
for production in productions:
    file  = "/home/s3608936/Code/DiHiggsAnalysisR21/run/DiHiggsPlots/{}/Merged.root".format(production)
    f.append(ROOT.TFile(file))

    for r in regions:
        h_signif = ROOT.TH1F(production+r, production+r, len(signal_samples), 0, len(signal_samples))
        ROOT.SetOwnership(h_signif, False)
        hmap[production+r] = h_signif
        print production, r

        N_bkg  = 0    
        for b in bkg_samples: 
            # print b
            h_bkg = f[-1].Get("{}_{}".format(b, r))
            ROOT.SetOwnership(h_bkg, False)
            if h_bkg == None: 
                # print "skip..."
                continue
            N_bkg += h_bkg.Integral()

        for i in range(len(signal_samples)):
            s = signal_samples[i]
            h_signif.GetXaxis().SetBinLabel(i+1, "M = {} GeV".format(signal_masses[i]))
            # print s
            h_sig = f[-1].Get("{}_{}".format(s, r))
            ROOT.SetOwnership(h_sig, False)
            N_sig = h_sig.Integral()
            # print N_sig

            h_signif.SetBinContent(i+1, N_sig / sqrt(N_bkg))
            # print "N_bkg:", N_bkg, "N_sig:", N_sig, "signif:", N_sig / sqrt(N_bkg)



c = []
for r in regions:
    c.append(ROOT.TCanvas())
    c[-1].cd()
    print "   ", productions[0], productions[1], r
    hmap[productions[0]+r].GetYaxis().SetTitle("S / sqrt(B)")
    ymax = 1.4 * hmap[productions[0]+r].GetMaximum()
    hmap[productions[0]+r].GetYaxis().SetRangeUser(0, ymax)
    hmap[productions[0]+r].SetMarkerColor(ROOT.kBlue)
    hmap[productions[0]+r].SetLineColor(ROOT.kBlue)
    hmap[productions[0]+r].Draw("PL")
    hmap[productions[1]+r].Draw("SAMEPL")
    leg = ROOT.TLegend(0.2 , 0.74, 0.5, 0.9)
    leg.AddEntry(hmap[productions[0]+r], "fixed pT cut")
    leg.AddEntry(hmap[productions[1]+r], "trigger dependent pT cut")
    leg.Draw()

    c[-1].SaveAs("output/vetoStudies/significance_ptCut_{}.pdf".format(r))

