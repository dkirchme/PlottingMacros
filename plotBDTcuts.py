from lxml import etree
import os
import ROOT
from DKColor import DKColor

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
ROOT.SetAtlasStyle()

# ----------------------------------------------------------------------------
def get_significance_hist(hist_sig, hist_bkg):
   sgnfc_max = 0.
   x_max = 0.
   for x in range(1, hist_sig.GetNbinsX()+1):
      ns = hist_sig.Integral(0, x)
      nb = hist_bkg.Integral(0, x)
      if (nb == 0): continue
      # sgnfc = ns/(ns+nb) # statistical significance
      p = ns/(ns+nb)  # purity
      sgnfc = p * (1 - p)  # gini index      
      if (sgnfc > sgnfc_max):
         sgnfc_max = sgnfc
         x_max = x
   print x_max, sgnfc_max

   # plot spot of highest significance
   hist_sgnfc = ROOT.TH1F("hist_sgnfc_"+variables[i], "", 
                          hist_sig.GetNbinsX(),
                          hist_sig.GetXaxis().GetXmin(),
                          hist_sig.GetXaxis().GetXmax())
   hist_sgnfc.SetBinContent(x_max, 0.7*maxval)
   
   return hist_sgnfc

# ----------------------------------------------------------------------------
def repl(s):
   return s.replace("(", "_").replace(")", "_")

# ----------------------------------------------------------------------------
def my_min(l):
   if (len(l) == 0):
      return 0
   else:
      return min(l)

# ----------------------------------------------------------------------------
def my_max(l):
   if (len(l) == 0):
      return 0
   else:
      return max(l)



# ----------------------------------------------------------------------------
# ------------------------------------
# get xml from weights file
# ------------------------------------
# path_weights = "weights/TMVAClassification_BDT-TMVA-untransformed.weights.xml"
# path_tmva_file = "/ZIH.fast/users/kirchmeier/ID_results/TMVA-untransformed.root"
# path_weights = "weights/TMVAClassification_BDT-TMVA-logAbsS.weights.xml"
# path_tmva_file = "/ZIH.fast/users/kirchmeier/ID_results/TMVA-logAbsS.root"
# path_weights = "weights/TMVAClassification_BDT-TMVA-logAbsS-logMtrack.weights.xml"
# path_tmva_file = "/ZIH.fast/users/kirchmeier/ID_results/TMVA-logAbsS-logMtrack.root"

# training_name = "BDT-logAbsS-logMtrack-logFiso"
# training_name = "BDT_var_all"
# training_name = "BDT_var_all_log"
# training_name = "BDT_logAbsS_logMtrack_logFiso"
training_name = "BDT_final"
base_path = "/ZIH.fast/users/kirchmeier/ID_results/"
path_weights = base_path+"weights/TMVAClassification_"+training_name+".weights.xml"
path_tmva_file = "/ZIH.fast/users/kirchmeier/ID_results/"+training_name+".root"

f = open(path_weights)
xml = etree.parse(f)

# ------------------------------------
# initialize dicts for variables and cuts
# ------------------------------------
nvar = int(xml.find(".//Variables").get("NVar"))
variables = { i : "" for i in range(nvar) }
cuts = { i : [] for i in range(nvar) }

# ------------------------------------
# get variable names from xml
# ------------------------------------
for var in xml.findall(".//Variable"):
   ivar = int(var.get("VarIndex"))
   name = var.get("Expression")
   variables[ivar] = name.replace(")", "_").replace("(", "_")

# ------------------------------------
# get all cuts from xml
# ------------------------------------
for node in xml.findall(".//Node"):
   ivar = int(node.get("IVar"))
   if (ivar == -1): continue
   cut = float(node.get("Cut"))
   cuts[ivar].append(cut)

# ------------------------------------
# initalize histograms and canvas
# ------------------------------------
cut_hists = { i : ROOT.TH1F(variables[i], 
                        ";{}".format(variables[i]), 
                        100, my_min(cuts[i]), my_max(cuts[i])) for i in range(nvar) }
canvas = { i : ROOT.TCanvas() for i in range(nvar) }

# ------------------------------------
# fill cut histograms
# ------------------------------------
for i in range(nvar):
   for cut in cuts[i]:
      cut_hists[i].Fill(cut)

# ------------------------------------
# get file that contains variable distributions
# ------------------------------------
tmva_file = ROOT.TFile(path_tmva_file)
t = tmva_file.Get("TrainTree")

# ------------------------------------
# plot variable and cut distributions
# ------------------------------------
for i in range(nvar):
   # i = 0
   canvas[i].cd()
   t.Draw(variables[i]+">>hist_sig_"+variables[i], "isSignal==1", "NORM")
   t.Draw(variables[i]+">>hist_bkg_"+variables[i], "isSignal<=0", "NORMSAME")

   hist_sig = ROOT.gDirectory.Get("hist_sig_"+variables[i])
   hist_bkg = ROOT.gDirectory.Get("hist_bkg_"+variables[i])

   hist_sig.SetLineColor(DKColor['Blue'])
   hist_sig.GetXaxis().SetTitle(variables[i])
   hist_bkg.SetLineColor(DKColor['Red'])


   cut_hists[i].SetLineColor(DKColor['Green'])
   if cut_hists[i].Integral() != 0:
      cut_hists[i].Scale(1. / cut_hists[i].Integral())

   maxval = 1.4 * max( hist_sig.GetMaximum(), 
                       hist_bkg.GetMaximum(), 
                       cut_hists[i].GetMaximum() )
   hist_sig.GetYaxis().SetRangeUser(0, maxval)

   cut_hists[i].Draw("SAME")
   hist_bkg.Draw("SAME")
   hist_sig.Draw("SAME")

   # calculate significance
   # hist_sgnfc = get_significance_hist(hist_sig, hist_bkg)
   # hist_sgnfc.Draw("SAME")

   leg = ROOT.TLegend(0.6 , 0.74, 0.9, 0.9)
   leg.SetFillColor(0)
   leg.SetLineColor(0)

   ROOT.SetOwnership(leg, False)

   leg.AddEntry(hist_sig, 'di-tau', 'l')
   leg.AddEntry(hist_bkg, 'QCD jet', 'l')
   leg.AddEntry(cut_hists[i], 'BDT cut', 'l')

   leg.Draw('NB')


   canvas[i].Update()

   # canvas[i].Print('./output/id-results/vardist-{}-{}.png'.format(training_name, variables[i]))
