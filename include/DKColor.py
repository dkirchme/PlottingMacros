################################################################################
# * Processes the GraphHolder
################################################################################

# python imports 
import array

# ROOT imports
import ROOT

def getBlueToRed():
   Number = 3
   Red    = array.array('d', [ 79./255.,  31./255., 255./255.])
   Green  = array.array('d', [214./255., 103./255.,  80./255.])
   Blue   = array.array('d', [214./255., 138./255.,  78./255.])
   Length = array.array('d', [0.00,        0.30,       1.00  ])
   nb=255;
   fi = ROOT.TColor.CreateGradientColorTable(Number,Length,Red,Green,Blue,nb)
   pal = array.array('i', [fi+i for i in xrange(0, nb)])
   return (nb, pal)

def getWhiteToBlue():
   Number = 2
   Red    = array.array('d', [255./255.,  31./255.])
   Green  = array.array('d', [255./255., 103./255.])
   Blue   = array.array('d', [255./255., 138./255.])
   Length = array.array('d', [   0.00,      1.00  ])
   nb=255;
   fi = ROOT.TColor.CreateGradientColorTable(Number,Length,Red,Green,Blue,nb)
   pal = array.array('i', [fi+i for i in xrange(0, nb)])
   return (nb, pal)


def getWhiteToRed():
   Number = 2
   Red    = array.array('d', [255./255., 255./255.])
   Green  = array.array('d', [255./255.,  80./255.])
   Blue   = array.array('d', [255./255.,  78./255.])
   Length = array.array('d', [0.00,         1.00  ])
   nb=255;
   fi = ROOT.TColor.CreateGradientColorTable(Number,Length,Red,Green,Blue,nb)
   pal = array.array('i', [fi+i for i in xrange(0, nb)])
   return (nb, pal)


def getRedToWhiteToBlue():
   Number = 4
   Red    = array.array('d', [  31./255., 255./255., 255./255., 255./255.])
   Green  = array.array('d', [ 103./255., 255./255., 255./255.,  80./255.])
   Blue   = array.array('d', [ 138./255., 255./255., 255./255.,  78./255.])
   Length = array.array('d', [      0.00,       0.4,       0.6,       1.0])
   nb=255;
   fi = ROOT.TColor.CreateGradientColorTable(Number,Length,Red,Green,Blue,nb)
   pal = array.array('i', [fi+i for i in xrange(0, nb)])

   return (nb, pal)

DKColor = dict()

DKColor['Red'] = 3001
DKColor['DarkBlue'] = 3002
DKColor['Blue'] = 3003
DKColor['LightBlue'] = 3004
DKColor['Green'] = 3005
DKColor['Pink'] = 3006
DKColor['Purple'] = 3007
DKColor['LightGray'] = 3008
DKColor['Black'] = 3009
DKColor['White'] = 3030
DKColor['LightGreen'] = 3031

DKColor['Red1'] = 3001
DKColor['Red2'] = 3010
DKColor['Red3'] = 3011
DKColor['Red4'] = 3012

DKColor['Blue1'] = 3013
DKColor['Blue2'] = 3014
DKColor['Blue3'] = 3015
DKColor['Blue4'] = 3016

DKColor['RB0'] = 3020
DKColor['RB1'] = 3021
DKColor['RB2'] = 3022
DKColor['RB3'] = 3023
DKColor['RB4'] = 3024
DKColor['RB5'] = 3025
DKColor['RB6'] = 3026
DKColor['RB7'] = 3027
DKColor['RB8'] = 3028
DKColor['RB9'] = 3029

DKColor['HHMediumPink']   = 3040  
DKColor['HHDarkBlue']   = 3041  
DKColor['HHMediumTurq'] = 3042 
DKColor['HHDarkTurq']   = 3043  
DKColor['HHLightTurq']  = 3044  
DKColor['HHOffWhite']   = 3045  
DKColor['HHDarkYellow'] = 3046 
DKColor['HHLightPink']  = 3047
DKColor['HHDarkPink']   = 3048 

DKRainbow = [DKColor["RB{}".format(i)] for i in range(10)]



col1 = ROOT.TColor(DKColor['Red'],         255./255,  80./255,  78./255)
col2 = ROOT.TColor(DKColor['DarkBlue'],     14./255,  67./255,  82./255)
col3 = ROOT.TColor(DKColor['Blue'],         31./255, 103./255, 138./255)
col4 = ROOT.TColor(DKColor['LightBlue'],    79./255, 214./255, 214./255)
col5 = ROOT.TColor(DKColor['Green'],        98./255, 245./255, 117./255)
col6 = ROOT.TColor(DKColor['Purple'],       59./255,  69./255, 217./255)
col7 = ROOT.TColor(DKColor['Pink'],        204./255,  47./255, 125./255)
col8 = ROOT.TColor(DKColor['LightGray'],   220./255, 220./255, 220./255)
col9 = ROOT.TColor(DKColor['Black'],             0.,       0.,       0.)
col10= ROOT.TColor(DKColor['White'],             1.,       1.,       1.)
col11= ROOT.TColor(DKColor['LightGreen'],  194./255, 245./255, 200./255)

# col10 = ROOT.TColor(DKColor['Red1'],       255./255,  80./255,  78./255)
col11 = ROOT.TColor(DKColor['Red2'],       191./255,  60./255,  58./255)
col12 = ROOT.TColor(DKColor['Red3'],       127./255,  40./255,  39./255)
col13 = ROOT.TColor(DKColor['Red4'],        64./255,  20./255,  19./255)

col14 = ROOT.TColor(DKColor['Blue1'],       57./255, 190./255, 255./255)
col15 = ROOT.TColor(DKColor['Blue2'],       43./255, 143./255, 191./255)
col16 = ROOT.TColor(DKColor['Blue3'],       29./255,  95./255, 127./255)
col17 = ROOT.TColor(DKColor['Blue4'],       14./255,  48./255,  64./255)

col20 = ROOT.TColor(DKColor['RB0'],        124./255, 120./255, 255./255)
col21 = ROOT.TColor(DKColor['RB1'],        018./255, 179./255, 255./255)
col22 = ROOT.TColor(DKColor['RB2'],        255./255, 194./255, 108./255)
col23 = ROOT.TColor(DKColor['RB3'],        255./255, 056./255, 007./255)
col24 = ROOT.TColor(DKColor['RB4'],        232./255, 097./255, 148./255)
col25 = ROOT.TColor(DKColor['RB5'],        142./255, 004./255, 232./255)
col26 = ROOT.TColor(DKColor['RB6'],        098./255, 232./255, 198./255)
col27 = ROOT.TColor(DKColor['RB7'],        009./255, 232./255, 005./255)
col28 = ROOT.TColor(DKColor['RB8'],        216./255, 255./255, 097./255)
col29 = ROOT.TColor(DKColor['RB9'],        255./255, 205./255, 004./255)

col40 = ROOT.TColor(DKColor['HHMediumPink'],  242./255,  56./255,  90./255)
col41 = ROOT.TColor(DKColor['HHDarkBlue'],   52./255,  56./255,  68./255)
col42 = ROOT.TColor(DKColor['HHMediumTurq'], 54./255, 177./255, 191./255)
# col43 = ROOT.TColor(DKColor['HHDarkTurq'],    0./255, 143./255, 143./255)
col43 = ROOT.TColor(DKColor['HHDarkTurq'],   49./255, 161./255, 173./255)
col44 = ROOT.TColor(DKColor['HHLightTurq'],  74./255, 217./255, 217./255)
col45 = ROOT.TColor(DKColor['HHOffWhite'],  233./255, 241./255, 223./255)
# col46 = ROOT.TColor(DKColor['HHDarkYellow'],253./255, 187./255,  54./255)
col46 = ROOT.TColor(DKColor['HHDarkYellow'],253./255, 194./255,   0./255)
col47 = ROOT.TColor(DKColor['HHLightPink'], 255./255, 135./255, 157./255)
col48 = ROOT.TColor(DKColor['HHDarkPink'],  115./255,  61./255,  71./255)


DKColor['BlueToRed'] = getBlueToRed()
DKColor['RedWhiteBlue'] = getRedToWhiteToBlue()
DKColor['WhiteToRed'] = getWhiteToRed()
DKColor['WhiteToBlue'] = getWhiteToBlue()

