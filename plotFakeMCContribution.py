import ROOT
from AtlasStyle import AtlasStyle
style = AtlasStyle()
style.SetStyle()
f = ROOT.TFile("/scratch/ws/s3608936-DiHiggsAnalysis/FakeFactorCalc/FFqcd_boosted-v10_calcFF_Rv65.root", "READ")
hp1 = f.Get("h_pass_data_Pt_SS_0tag")
hp2 = f.Get("h_pass_Pt_SS_0tag")

from array import array
bins = array('d', [300, 400, 450, 600, 1500])
hp1 = hp1.Rebin(len(bins)-1, hp1.GetName()+'rebin', bins)
hp2 = hp2.Rebin(len(bins)-1, hp2.GetName()+'rebin', bins)

hp2.SetMarkerColor(ROOT.kRed)
hp2.SetLineColor(ROOT.kRed)

hp1.SetMinimum(0)
hp1.SetMaximum(1.4*hp1.GetMaximum())

hp1.GetXaxis().SetTitle("Di-tau p_{T} [GeV]")
hp1.GetYaxis().SetTitle("Number of events")


c = ROOT.TCanvas()
hp1.Draw()
hp2.Draw("SAME")

leg = ROOT.TLegend(0.51, 0.75, 0.91, 0.90)
leg.AddEntry(hp1, "SS pass ID (data)")
leg.AddEntry(hp2, "SS pass ID (data - MC)")
leg.Draw('NB')


c.SaveAs("./output/fakeMCContribution/passID.pdf")