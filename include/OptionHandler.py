################################################################################
# * Base option handler class
# * Important: Default options have to be set *before* setting custom
#   options via SetOptions.
################################################################################

# python includes
import sys
from collections import OrderedDict

# common includes
from MacroLogger import *
from YAMLHandler import YAMLHandler
# from EmbeddedIPython import breakpoint as bp


################################################################################
class OptionHandler(BaseLoggerClass):
    #_______________________________________
    def __init__(self, name="OptionHandler", logLevel="ERROR"):
        BaseLoggerClass.__init__(self, name, logLevel)
        self.name = name
        self.options = OrderedDict()
        self._defaultOptions = OrderedDict()

    #_______________________________________
    def Reset(self):
        if not self._defaultOptions:
            self.logger.warning("In '%s' no default options set, nothing to reset." % self.name)
        self.SetOptions(**self._defaultOptions)

    #_______________________________________
    def AddOptions(self, **addOptions):
        if not self._defaultOptions:
            self.logger.warning("In '%s' no default options set. Set default before adding new options!" % self.name)
        for key, value in addOptions.items():
            self.options[key] = value
            self._defaultOptions[key] = value

    #_______________________________________
    def SetOptions(self, **kwargs):
        # if not self._defaultOptions:
        #     self.logger.error("In '%s' no default options set. Nothing to set." % self.name)

        for key, value in kwargs.items():
            # if not key in self.options:
            #     self.logger.error('This key is not set as option before: {}'.format(key))

            self.options[key] = value

    #_______________________________________
    def SetOptionsByYAML(self, yaml_file):
        # if not self._defaultOptions:
        #     self.logger.error("In '%s' no default options set. Nothing to set." % self.name)
        yH = YAMLHandler(logLevel=self._logLevel)
        self.logger.debug("Open yaml file %s" % yaml_file)

        try:
            config = yH.ReadYAML(yaml_file)
            self.SetOptions(**config)
        except:
            self.logger.error("Failure during loading of file %s, no options set." % yaml_file)
            raise

    #_______________________________________
    def SetDefaultOptions(self, **kwargs):
        if not kwargs:
            self.logger.error("Can not set default options in %s. Provided dict was empty." % self.name)

        for key, value in kwargs.items():

            self.options[key] = value
            self._defaultOptions[key] = value

    #_______________________________________
    def SetDefaultOptionsByYAML(self, yaml_file):
        yH = YAMLHandler(logLevel=self._logLevel)
        self.logger.debug("Open yaml file %s" % yaml_file)

        try:
            config = yH.ReadYAML(yaml_file)
            self.SetDefaultOptions(**config)
        except:
            self.logger.error("Failure during loading of file %s, no options set." % yaml_file)
            raise

    #_______________________________________
    def DefaultOptions(self):
        return self._defaultOptions

    #_______________________________________
    def Options(self, *key):
        if key:
            key = key[0]
            try:
                return self.options[key]
            except:
                self.logger.error('key error. Key {} not found in Options'.format(key))
                raise
        else:
            return self.options

    #_______________________________________
    def __getitem__(self, key):
        try:
            return self.options[key]
        except KeyError:
            self.logger.error("Key '%s' not found in options." % key)
            raise
        except:
            self.logger.error("Unexpected error: %s" % sys.exc_info()[0])
            raise

    #_______________________________________
    def __setitem__(self, key, value):
        if key in self.options:
            self.options[key] = value
        else:
            self.logger.warning("Key '%s' not found in options, nothing to set." % key)

    #_______________________________________
    def __repr__(self):
        return self.options.__repr__()

    #_______________________________________
    def __str__(self):
        return self.options.__str__()

    #_______________________________________
    def __contains__(self, item):
        return (item in self.options.keys())


################################################################################
class BaseOptionClass(object):
    """
    Basic option class: all classes using option handler should derive from this class
    """
    #_______________________________________
    def __init__(self, name, logLevel):
        # set main option handler
        self.options = OptionHandler(name + "_OptionHandler", logLevel)

        # internal objects
        self._listOfOptionsToEvaluate = []

    #_______________________________________
    # Option access functions
    #_______________________________________

    #_______________________________________
    def AddOptions(self, **addOptions):
        self.options.AddOptions(**addOptions)

    #_______________________________________
    def SetOptionsToEvaluate(self, *optionKeys):
        for key in optionKeys:
            if key not in self._listOfOptionsToEvaluate:
                self._listOfOptionsToEvaluate.append(key)

        self._EvaluateOptionsByList(self.options._defaultOptions)
        self._EvaluateOptionsByList(self.options.options)

    #_______________________________________
    def SetOptions(self, **kwargs):
        self.options.SetOptions(**kwargs)
        self._EvaluateOptionsByList(self.options.options)

    #_______________________________________
    def SetOptionsByYAML(self, yaml_file):
        self.options.SetOptionsByYAML(yaml_file)
        self._EvaluateOptionsByList(self.options.options)

    #_______________________________________
    def SetDefaultOptions(self, **kwargs):
        self.options.SetDefaultOptions(**kwargs)
        self._EvaluateOptionsByList(self.options._defaultOptions)

    #_______________________________________
    def SetDefaultOptionsByYAML(self, yaml_file):
        self.options.SetDefaultOptionsByYAML(yaml_file)
        self._EvaluateOptionsByList(self.options._defaultOptions)

    #_______________________________________
    def SetOptionsBySubset(self, options, key):
        bp()
        self.options.SetDefaultOptions(options[key])

    #_______________________________________
    def ResetOptions(self):
        self.options.Reset()

    #_______________________________________
    def DefaultOptions(self):
        return self.options.DefaultOptions()

    #_______________________________________
    def Options(self, *key):
        return self.options.Options(*key)


    #_______________________________________
    # Internal functions
    #_______________________________________

    #_______________________________________
    def _EvaluateOptionsByList(self, optionDict):
        for option in self._listOfOptionsToEvaluate:
            self._EvaluateOption(option, optionDict)

    #_______________________________________
    def _EvaluateOption(self, optionName, optionDict):
        if optionName in optionDict:
            value = optionDict[optionName]
            if isinstance(value, str):
                optionDict[optionName] = eval(value)
