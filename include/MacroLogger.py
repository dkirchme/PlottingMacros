################################################################################
# * Tool to provide a common logger for macros
#   input: name  - name of the logging instance
#          level - severity level for displayed messages
#   output: logging  instance of type logging.Logger
################################################################################

# python include(s)
import logging

# common include(s)
import CSIColorTool


################################################################################
class ColoredFormatter(logging.Formatter):
    #_______________________________________
    def __init__(self, fmt, use_color=True, use_bold_name=True):
        logging.Formatter.__init__(self, fmt)
        self._use_color = use_color
        self._use_bold_name = use_bold_name
        self.__setcolorlevel()

    #_______________________________________
    def __setcolorlevel(self):
        self._color_level = {"DEBUG"   : ["BLUE"],
                             "INFO"    : ["ENDC"],
                             "WARNING" : ["DARK_MAGENTA"],
                             "ERROR"   : ["RED"],
                             "CRITICAL": ["DARK_RED", "LIGHT_AUQA_BG"]}

    #_______________________________________
    def format(self, record):
        levelname = record.levelname
        if self._use_color and levelname in self._color_level:
            colors = self._color_level[levelname]

            record.levelname = CSIColorTool.ColorStr(levelname, *colors)
            record.msg = CSIColorTool.ColorStr(str(record.msg), *colors)
            record.module = CSIColorTool.ColorStr(record.module, *colors)
            record.funcName = CSIColorTool.ColorStr(record.funcName, *colors)
            record.filename = CSIColorTool.ColorStr(record.filename, *colors)
            record.name = CSIColorTool.ColorStr(record.name, *colors)

        if self._use_bold_name:
            record.name = CSIColorTool.ColorStr(record.name, "BOLD")

        return logging.Formatter.format(self, record)


################################################################################
def GetLogger(name, level, use_color=False, use_bold_name=False):
    logger = logging.getLogger(name)

    hdl = logging.StreamHandler()

    form_str = '%(name)s:%(funcName)s:%(lineno)s %(levelname)s: %(message)s'
    if use_color or use_bold_name:
        form = ColoredFormatter(form_str, use_color, use_bold_name)
    else:
        form = logging.Formatter(form_str)
        #form_str = '{}:%(funcName)s:%(lineno)s %(levelname)s: %(message)s'.format(CSIColorTool.ColorStr('%(name)s', 'BOLD'))
        #form = logging.Formatter(form_str)

    hdl.setFormatter(form)

    logger.addHandler(hdl)
    logger.setLevel(eval('logging.' + level))
    logger.propagate = 0

    return logger


def SetLoggerLevel(loggerObject, level):
    """
    Input: logging object, level string e.g. 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'
    """
    loggerObject.setLevel(eval('logging.' + level))


################################################################################
class BaseLoggerClass(object):
    #_______________________________________
    def __init__(self, name, logLevel, use_color=True, use_bold_name=True):
        self.logger = GetLogger(name, logLevel, use_color, use_bold_name)
        self.logLevel = logLevel

    #_______________________________________
    def SetLoggerLevel(self, logLevel):
        SetLoggerLevel(self.logger, logLevel)

    #_______________________________________
    def GetLoggerLevel(self):
        return self.logLevel
