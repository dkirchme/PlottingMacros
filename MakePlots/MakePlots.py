#!/usr/bin/env python

################################################################################
# * Wrapper to call MainPlotter tool using argparse
################################################################################

# python includes
import sys
import argparse
import os
import multiprocessing

# common includes
import ROOT

# plot macro includes
from MainPlotter import MainPlotter
from PlotHolder import PlotHolder
from PlotHolder import GraphHolder
from MacroLogger import GetLogger
from MacroParser import GetLoggerParser
from YAMLHandler import YAMLHandler
# from EmbeddedIPython import breakpoint as bp
from ConfigHandler import ConfigHandler


def makeLogger():
    global logger
    logger = GetLogger("MakePlots", 'DEBUG')


#_______________________________________
def main(argv):
    # setup argparse
    logger_parser = GetLoggerParser(default_level="INFO")
    desc = "Macro to plot validation variables for DiTau Reconstruction."

    aP = argparse.ArgumentParser(description=desc, parents=[logger_parser],
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    aP.add_argument('-c', '--config',
                    type=str,
                    default='../config/config.yaml',
                    help='Path to custom yaml config file.')

    aP.add_argument('-d', '--default_config',
                    type=str,
                    default='../config/default-config.yaml',
                    help='Path to default yaml config file.')

    aP.add_argument('-o', '--output_path',
                    type=str,
                    default='../output/',
                    help='Output path.')

    aP.add_argument('-O', '--output_file',
                    type=str,
                    default='output.root',)

    aP.add_argument('-b', '--batchmode',
                    type=int,
                    default=1,
                    help='Set batch mode. 1 for on, 0 for off.')
    aP.add_argument('--saveAs',
                    type=str,
                    default='pdf',
                    help='Set format to save plot as picture. If empty only a .root file is saved.')

    aP.add_argument('--save_all_hists',
                    default=False, 
                    action='store_true',
                    help='save all temporary histograms')

    aP.add_argument('--no_parallel',
                    default=False, 
                    action='store_true',
                    help='not using parallel mode')

    aP.add_argument('--old_style',
                    default=False, 
                    action='store_true',
                    help='use my custom old style')


    args = aP.parse_args()

    global logger
    logger = GetLogger("MakePlots", args.loggerlevel)
    logger.debug(vars(args))

    ROOT.gROOT.SetBatch(args.batchmode)
    f_out = createOutputFile(args.output_path + args.output_file)

    if (args.old_style):
        from AtlasStyle import AtlasStyle
        style = AtlasStyle(logLevel=args.loggerlevel)
        style.SetStyle()
    else:
        ROOT.gROOT.LoadMacro("../include/AtlasStyle.C") 
        ROOT.SetAtlasStyle()
        ROOT.gStyle.SetLegendFont(42)

    config = ConfigHandler(name='ConfigHandler', logLevel=args.loggerlevel)
    if (args.old_style):
        config.SetConfigsByYaml(custom=args.config, default='../config/default-config-old.yaml')
    else:
        config.SetConfigsByYaml(custom=args.config, default=args.default_config)


    if (args.no_parallel):
        for plot in config.GetConfig():
            logger.debug("start plotting {}".format(plot))

            mainPlotter = MainPlotter(name=plot, logLevel=args.loggerlevel)

            mainPlotter.SetOptions(**config.GetConfig()[plot])

            mainPlotter.Plot(f_out, args.output_path, args.saveAs, args.save_all_hists)
    else: 
        pool = multiprocessing.Pool(processes=10)
        for plot in config.GetConfig():
            pool.apply_async(makePlot, args=(args, plot, config.GetConfig()[plot]))

        pool.close()
        pool.join()

        # add all temp root files into one
        hadd_command = "hadd -f " + args.output_path + args.output_file
        hadd_command += " " + args.output_path + "tmp-*.root"
        print hadd_command
        os.system(hadd_command)

        # delete temp root files 
        del_command = "rm " + args.output_path + "tmp-*.root"
        print del_command
        os.system(del_command)


#_______________________________________
def makePlot(args, plot, plot_options):
    logger.info("start plotting {}".format(plot))

    f_out = createOutputFile(args.output_path + "tmp-{}.root".format(plot) )

    mainPlotter = MainPlotter(name=plot, logLevel=args.loggerlevel)
    mainPlotter.SetOptions(**plot_options)
    mainPlotter.Plot(f_out, args.output_path, args.saveAs, args.save_all_hists)

    f_out.Close()


#_______________________________________
def createOutputFile(outFile):
    logger.info("Create output file {}".format(outFile))
    try:
        f_out = ROOT.TFile(outFile, 'recreate')
    except:
        logger.error("Createing {} failed".format(outFile))
        raise

    return f_out


#_______________________________________
# def get_conf_from_yaml(yaml_file):
#     yH = YAMLHandler()
#     logger.debug("Open yaml file {}".format(yaml_file))
#     try:
#         config = yH.ReadYAML(yaml_file)
#     except:
#         logger.error("Failure during loading of file {}, no options set.".format(yaml_file))
#         raise

#     return config


#_______________________________________
if __name__ == "__main__":
    main(sys.argv[1:])
