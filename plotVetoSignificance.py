#!/usr/bin/python

import ROOT
import os
import sys
from math import sqrt
# from DKColor import getWhiteToBlue

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# file = "/ZIH.fast/users/kirchmeier/Run/run10/data-DTNT/user.dkirchme.mc15_13TeV.303366.RS_G_hh_bbtt_hh_c10_M1800.recon.AOD.e4438_s2608_r6869_v02.root"
file = "/home/s3608936/Code/DiHiggsAnalysisR21/run/DiHiggsPlots/submitDir_v9_Rv38/Merged.root"

f = ROOT.TFile(file)
# t = f.Get("tree")

# ROOT.gStyle.SetPaintTextFormat(".3f")
# n_colors, palette = getWhiteToBlue()
# ROOT.gStyle.SetPalette(n_colors, palette)


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
signal_masses = [1000, 1200, 1400, 1600, 1800, 2000, 2500, 3000]
signal_samples = ["Xtohh{}_Hw".format(m) for m in signal_masses]

bkg_samples = ["stopWt", "stops", "stopt", "WWPw", "WZPw", "ZZPw", "Wl", "Wcl", "Wcc", "Wbl", "Wbc", "Wbb", "ttbar_nonallhad", "ttbar_allhad", "fakes", "Zee_Sh221", "Zl", "Zcl", "Zcc", "Zbl", "Zbc", "Zbb"]


regions = ["2tag2pjet_0ptv_SR_Additional1btagFJ", "2tag2pjet_0ptv_SR_Additional2btagFJ", "2tag2pjet_0ptv_SR_SelFJVRJetOverlap", "1tag2pjet_0ptv_SR_Additional1btagFJ", "1tag2pjet_0ptv_SR_Additional2btagFJ", "1tag2pjet_0ptv_SR_SelFJVRJetOverlap"]
# regions = ["2tag2pjet_0ptv_SR_Additional1btagFJ"]


hmap = {}
# Nmap = {}

for r in regions:
    # heff = ROOT.TH1F(regions[j], regions[j], len(signal_samples), 0, len(signal_samples))
    hsig_tot = ROOT.TH1F(r+"tot", r+"tot", len(signal_samples), 0, len(signal_samples))
    hsig_pas = ROOT.TH1F(r+"pas", r+"pas", len(signal_samples), 0, len(signal_samples))
    hmap[r+"tot"] = hsig_tot
    hmap[r+"pas"] = hsig_pas
    print r
    # Nmap[r] = {}


    Ntot_bkg  = 0    
    Npass_bkg = 0
    for b in bkg_samples: 
        # print b
        h_bkg = f.Get("{}_{}".format(b, r))
        if h_bkg == None: 
            # print "skip..."
            continue
        Ntot_bkg += h_bkg.Integral()
        # print Ntot_bkg
        Npass_bkg += h_bkg.GetBinContent(1)
        # print Npass_bkg

    # Nmap[r]["bkg_tot"] = Ntot_bkg
    # Nmap[r]["bkg_pas"] = Npass_bkg

    for i in range(len(signal_samples)):
        s = signal_samples[i]
        hsig_tot.GetXaxis().SetBinLabel(i+1, "M = {} GeV".format(signal_masses[i]))
        hsig_pas.GetXaxis().SetBinLabel(i+1, "M = {} GeV".format(signal_masses[i]))
        print s
        h_sig = f.Get("{}_{}".format(s, r))
        Ntot_sig = h_sig.Integral()
        # Nmap[r][s+"_tot"] = Ntot_sig
        print Ntot_sig
        Npass_sig = h_sig.GetBinContent(1)
        # Nmap[r][s+"_pas"] = Npass_sig
        print Npass_sig
        # heff.SetBinContent(i+1, Npass_sig/Ntot_sig)

        hsig_tot.SetBinContent(i+1, Ntot_sig / sqrt(Ntot_bkg))
        hsig_pas.SetBinContent(i+1, Npass_sig / sqrt(Npass_bkg))
        print "Ntot_bkg:", Ntot_bkg, "Ntot_sig:", Ntot_sig, "sig:", Ntot_sig / sqrt(Ntot_bkg)



c = []
for r in regions:
    c.append(ROOT.TCanvas())
    c[-1].cd()
    # hmap[r].GetYaxis().SetRangeUser(0.9, 1.01)
    hmap[r+"pas"].GetYaxis().SetTitle("S / sqrt(B)")
    # hmap[r+"tot"].SetMarkerColor(ROOT.kBlack)
    hmap[r+"pas"].SetMarkerColor(ROOT.kBlue)
    hmap[r+"pas"].SetLineColor(ROOT.kBlue)
    hmap[r+"tot"].Draw("PL")
    hmap[r+"pas"].Draw("SAMEPL")

    c[-1].SaveAs("output/vetoStudies/significance_{}.pdf".format(r))

