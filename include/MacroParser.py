################################################################################
# * Tool to provide a common parser for macros
################################################################################

import argparse


def GetLoggerParser(attribute_short='-l',
                    attribute_long='--loggerlevel',
                    default_level='WARNING'):
    """
    Return a default parser for logger.
    """
    logger_parser = argparse.ArgumentParser(add_help=False)
    logger_parser.add_argument(attribute_short, attribute_long, type=str, default=default_level,
                               choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                               help='Set logger level.')
    return logger_parser


def GetByteConverterParser(prefix_short='-p',
                           prefix_long='--prefix',
                           prefix_default='B',
                           human_short='-H',
                           human_long='--humanReadable'):
    """
    Return a default parser to use with ByteConverter tool
    """
    bc_parser = argparse.ArgumentParser(add_help=False)
    bc_parser.add_argument('-p', '--prefix', type=str, default='B', choices=['B', 'KB', 'MB', 'GB', 'TB'], help='Fix unit prefix for the size values. Will be ignored if --humanReadable option is set.')
    bc_parser.add_argument('-H', '--humanReadable', action='store_true', help='Set the size values in a human readable format.')

    return bc_parser
