import inspect
from IPython.terminal.embed import InteractiveShellEmbed

ipshell = InteractiveShellEmbed()


def breakpoint():
    frame = inspect.currentframe().f_back
    msg = 'Stopped at {0.f_code.co_filename} at line {0.f_lineno}'.format(frame)

    # Go back one level!
    # This is needed because the call to ipshell is inside the function ipsh()
    ipshell(msg, stack_depth=2)
