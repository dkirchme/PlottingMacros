#!/usr/bin/python

import ROOT
import os
import time
import math

import glob
import itertools
from collections import OrderedDict
# from EmbeddedIPython import breakpoint as bp
from DKColor import DKColor

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

ROOT.gStyle.SetPaintTextFormat('5.3f')
ROOT.gStyle.SetMarkerSize(1.6)

  
def main():
    start_time = time.time()
    systematics = ['NOMINAL', 'ALT_GEO', 'IBL_30', 'PP0_50', 'QGSP_BIC']
    # systematics = ['NOMINAL']
    # systematics = ['NOMINAL', 'ALT_GEO']
    # systematics = ['NOMINAL', 'ALT_GEO', 'IBL_30']

    deltaR_regions = [
        # ('deltaR-00-02', ['deltaR>0', 'deltaR<0.2']),
        ('deltaR-02-10', ['deltaR>0.2', 'deltaR<1.0']),
        # ('deltaR-10-xx', ['deltaR>1.0'])
    ]
    leadPt_regions = [
        ('leadPt-00-200', ['lead_pt>0', 'lead_pt<200*1000']),
        ('leadPt-200-xx', ['lead_pt>200*1000'])
    ]
    sublPt_regions = [
        ('sublPt-00-20', ['subl_pt>0', 'subl_pt<20*1000']),
        ('sublPt-20-50', ['subl_pt>20*1000', 'subl_pt<50*1000']),
        ('sublPt-50-xx', ['subl_pt>50*1000'])
    ]

    working_points = OrderedDict()
    working_points['reco'] = '1'
    working_points['veryloose'] = 'ditau_JetBDT>0.6'
    working_points['loose'] = 'ditau_JetBDT>0.65'
    working_points['medium'] = 'ditau_JetBDT>0.72'
    working_points['tight'] = 'ditau_JetBDT>0.77'


    # constructs regions dictionary, like:
    # {'var1-w-x_var2-y-z': ['var1>w', 'var1<x', 'var2>y', 'var2<z']}
    regions = {'_'.join([reg[0] for reg in regs]) : sum([reg[1] for reg in regs], []) for regs in itertools.product(deltaR_regions, sublPt_regions, leadPt_regions)  }
    regions['deltaR-00-02'] = ['deltaR>0', 'deltaR<0.2']
    regions['deltaR-10-xx'] = ['deltaR>1.0']

    dic = OrderedDict()
    for s in systematics: dic[s] = {}

    dic['NOMINAL']['file_names'] = [ get_file_names(f) for f in [   
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3126_r9364_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3126_r9364_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['ALT_GEO']['file_names'] = [ get_file_names(f) for f in [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3155_r9476_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3155_r9476_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['IBL_30']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3156_r9477_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3156_r9477_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['PP0_50']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3157_r9478_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3157_r9478_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['QGSP_BIC']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3169_r9552_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3169_r9552_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3169_r9552_r9315.ntuple_v0_9_3.test02_2_hist/user.dkirchme.*.root',
    ]]


    dic['NOMINAL']['color'] = DKColor['RB1']
    dic['ALT_GEO']['color'] = DKColor['RB2']
    dic['IBL_30']['color'] = DKColor['RB3']
    dic['PP0_50']['color'] = DKColor['RB5']
    dic['QGSP_BIC']['color'] = DKColor['RB6']


    # ====================================
    # build systematics histograms
    # ====================================
    for (syst_name,syst) in dic.iteritems():
        # flatten file name lists
        syst['file_names'] = sum(syst['file_names'], [])

        # add all files to one tree
        syst['tree'] = ROOT.TChain()
        for f in syst['file_names']:
            syst['tree'].Add(f+'/Nominal/BaselineSelection_tree_APPLY_DITAU_BASESELECTION(kDiTaus,0)')

        # get histograms and efficiencies
        for (region_name, region) in regions.iteritems():
            cuts_all = ['true_ditau_'+ cut for cut in region]
            syst['hist_all_'+region_name] = get_hist_from_tree(syst_name+'_hist_all_'+region_name, syst['tree'], '1', cuts_all)

            for (wp_name, wp_cut) in working_points.iteritems():
                print syst_name, wp_name, region_name
                cuts_pas = ['ditau_truth_'+ cut for cut in region]
                cuts_pas.append(wp_cut)

                syst['hist_pas_'+wp_name+region_name] = get_hist_from_tree(wp_name+syst_name+'_hist_pas_'+region_name, syst['tree'], '1', cuts_pas)
                syst['eff_'+wp_name+region_name] = ROOT.TGraphAsymmErrors(syst['hist_pas_'+wp_name+region_name], syst['hist_all_'+region_name] )

    # =================
    # make plots 
    # =================
    cs = []
    for reg_name in regions.keys():
        for wp_name in working_points.keys():
            cs.append(ROOT.TCanvas(wp_name+reg_name, wp_name+reg_name))
            cs[-1].cd()
            for sys_name in dic.keys():
                dic[sys_name]['hist_all_'+reg_name].SetLineColor(dic[sys_name]['color'])
                dic[sys_name]['hist_all_'+reg_name].Draw('same')
                dic[sys_name]['hist_pas_'+wp_name+reg_name].SetLineColor(dic[sys_name]['color'])
                dic[sys_name]['hist_pas_'+wp_name+reg_name].Draw('same')

            cs.append(ROOT.TCanvas('eff_'+wp_name+reg_name, 'eff_'+wp_name+reg_name))
            cs[-1].cd()
            x = ROOT.Double()
            y = ROOT.Double()
            first = True
            for sys_name in dic.keys():
                if first: 
                    dic[sys_name]['eff_'+wp_name+reg_name].GetPoint(0, x, y)
                    dic[sys_name]['eff_'+wp_name+reg_name].GetYaxis().SetRangeUser(y-0.05, y+0.05)

                dic[sys_name]['eff_'+wp_name+reg_name].SetLineColor(dic[sys_name]['color'])
                dic[sys_name]['eff_'+wp_name+reg_name].SetMarkerColor(dic[sys_name]['color'])

                if first: dic[sys_name]['eff_'+wp_name+reg_name].Draw('apsame')
                else: dic[sys_name]['eff_'+wp_name+reg_name].Draw('psame')

                first = False


    # ================
    # make legend
    # ================
    cs.append(ROOT.TCanvas('legend', 'legend'))
    cs[-1].cd()
    leg = ROOT.TLegend(0.1, 0.1, 0.9, 0.9)    
    for sys_name in dic.keys():
        leg.AddEntry(dic[sys_name]['eff_'+wp_name+reg_name], sys_name, 'pl')
    leg.Draw()


    # =======================================
    # retrieve systematics from histograms
    # =======================================
    print ' '
    print ' '
    print '================================================='
    print '=               uncertainty values              ='
    print '================================================='
    x = ROOT.Double()
    for region_name in regions.keys():
        for wp_name in working_points.keys():
            print wp_name, region_name
            eff_nominal = dic['NOMINAL']['eff_'+wp_name+region_name]
            eff_nom = ROOT.Double()
            dic['NOMINAL']['eff_'+wp_name+region_name].GetPoint(0, x, eff_nom)
            dic['NOMINAL']['efficiency_'+wp_name+region_name] = eff_nom
            print 'nominal efficiency', eff_nom

            uncert_squared_total = 0
            for (syst_name,syst) in dic.iteritems():
                if syst_name is 'NOMINAL': continue
                eff = ROOT.Double()
                syst['eff_'+wp_name+region_name].GetPoint(0, x, eff)
                syst['uncert_'+wp_name+region_name] = abs(eff - eff_nom)
                uncert_squared_total += syst['uncert_'+wp_name+region_name]**2
                print syst_name, syst['uncert_'+wp_name+region_name]
            dic['NOMINAL']['uncert_'+wp_name+region_name] = math.sqrt(uncert_squared_total)
            # if eff_nom != 0 and math.sqrt(uncert_squared_total) < eff_nom:
            if eff_nom != 0:
                dic['NOMINAL']['reluncert_'+wp_name+region_name] = math.sqrt(uncert_squared_total) / eff_nom
            else:
                dic['NOMINAL']['reluncert_'+wp_name+region_name] = 1.

            print ('TOTAL %.6f' % (dic['NOMINAL']['uncert_'+wp_name+region_name]))
            print ('TOTAL relative %.6f' % (dic['NOMINAL']['reluncert_'+wp_name+region_name]))
            print '================================================='
        
    print ' '
    print ' '

    # =======================================
    # plot uncertainty values
    # =======================================
    h_uncert = ROOT.TH2F('uncertainties', 'uncertainties', len(working_points), 0, len(working_points), len(regions), 0, len(regions))
    h_eff = ROOT.TH2F('efficiencies', 'efficiencies', len(working_points), 0, len(working_points), len(regions), 0, len(regions))
    h_uncerts = OrderedDict()
    for syst in dic.keys():
        h_uncerts[syst] = ROOT.TH2F('uncert_'+syst, 'uncert_'+syst, len(working_points), 0, len(working_points), len(regions), 0, len(regions))
    h_total = OrderedDict()
    for syst in dic.keys():
        h_total[syst] = ROOT.TH2F('total_'+syst, 'total_'+syst, len(working_points), 0, len(working_points), len(regions), 0, len(regions))
    h_pass = OrderedDict()
    for syst in dic.keys():
        h_pass[syst] = ROOT.TH2F('pass_'+syst, 'pass_'+syst, len(working_points), 0, len(working_points), len(regions), 0, len(regions))
        

    # set labels
    i = 1
    for wp_name in working_points.keys():
        h_uncert.GetXaxis().SetBinLabel(i, wp_name)
        [h_uncerts[s].GetXaxis().SetBinLabel(i, wp_name) for s in h_uncerts.keys()]
        [h_total[s].GetXaxis().SetBinLabel(i, wp_name) for s in h_total.keys()]
        [h_pass[s].GetXaxis().SetBinLabel(i, wp_name) for s in h_pass.keys()]
        h_eff.GetXaxis().SetBinLabel(i, wp_name)
        i += 1
    i = 1
    for reg_name in reversed(sorted(regions.keys())):
        h_uncert.GetYaxis().SetBinLabel(i, convert_regname(reg_name))
        [h_uncerts[s].GetYaxis().SetBinLabel(i, convert_regname(reg_name)) for s in h_uncerts.keys()]
        [h_total[s].GetYaxis().SetBinLabel(i, convert_regname(reg_name)) for s in h_total.keys()]
        [h_pass[s].GetYaxis().SetBinLabel(i, convert_regname(reg_name)) for s in h_pass.keys()]
        h_eff.GetYaxis().SetBinLabel(i, convert_regname(reg_name))
        i += 1

    # fill histogram
    for wp_name in working_points.keys():
        for reg_name in regions.keys():
            h_uncert.Fill(wp_name, convert_regname(reg_name), dic['NOMINAL']['reluncert_'+wp_name+reg_name])
            [h_uncerts[s].Fill(wp_name, convert_regname(reg_name), dic[s]['uncert_'+wp_name+reg_name]) for s in h_uncerts.keys()]
            [h_total[s].Fill(wp_name, convert_regname(reg_name), dic[s]['hist_all_'+reg_name].GetEntries()) for s in h_total.keys()]
            [h_pass[s].Fill(wp_name, convert_regname(reg_name), dic[s]['hist_pas_'+wp_name+reg_name].GetEntries()) for s in h_pass.keys()]
            h_eff.Fill(wp_name, convert_regname(reg_name), dic['NOMINAL']['efficiency_'+wp_name+reg_name])


    # set color palette
    n_colors, palette = DKColor['WhiteToRed']
    ROOT.gStyle.SetPalette(n_colors, palette)

    # plot total relative uncertainties
    cs.append(ROOT.TCanvas('uncertainties', 'uncertainties', 1500, 1000))
    cs[-1].cd()
    cs[-1].SetLeftMargin(0.4)
    h_uncert.Draw('COLTEXT')

    # plot individual uncertainties
    for s in h_uncerts.keys():
        cs.append(ROOT.TCanvas('uncert_'+s, 'uncert_'+s, 1500, 1000))
        cs[-1].cd()
        cs[-1].SetLeftMargin(0.4)
        h_uncerts[s].Draw('COLTEXT')

    # plot number of total ditaus
    for s in h_total.keys():
        cs.append(ROOT.TCanvas('total_'+s, 'total_'+s, 1500, 1000))
        cs[-1].cd()
        cs[-1].SetLeftMargin(0.4)
        h_total[s].Draw('COLTEXT')

    # plot number of pass ditaus
    for s in h_pass.keys():
        cs.append(ROOT.TCanvas('pass_'+s, 'pass_'+s, 1500, 1000))
        cs[-1].cd()
        cs[-1].SetLeftMargin(0.4)
        h_pass[s].Draw('COLTEXT')

    # plot efficiencies
    cs.append(ROOT.TCanvas('efficiencies', 'efficiencies', 1500, 1000))
    cs[-1].cd()
    cs[-1].SetLeftMargin(0.4)
    h_eff.Draw('COLTEXT')

    # create output file    
    f = ROOT.TFile('output/systematic_results/systematics.root', 'RECREATE')
    for c in cs:
        c.Print('output/systematic_results/'+c.GetName()+'.pdf')
        c.Write()

    print('===============================')
    print('processing time: %.2f s' % (time.time() - start_time))
    print('===============================')

    # bp()

    f.Close()
    return


def get_file_names(s):
    files = glob.glob(s)
    if files == []: 
        print("WARNING: no files found with name {}".format(s))

    return files


def get_hist_from_tree(hist_name, tree, variable, cuts, nbins=1, xmin=0.5, xmax=1.5):
    cutexpr = ['({})'.format(c) for c in cuts]
    cutexpr = "*".join(cutexpr)
    h = ROOT.TH1F(hist_name, hist_name, nbins, xmin, xmax)

    tree.Project(hist_name, variable, cutexpr)
    return h

def convert_regname(reg_name):
    label = ''
    if 'deltaR-00-02' in reg_name: label += 'low #DeltaR'
    if 'deltaR-02-10' in reg_name: label += 'medium #DeltaR'
    if 'deltaR-10-xx' in reg_name: label += 'high #DeltaR'
    if 'leadPt-00-200' in reg_name: label += ', low p_{T}^{lead}'
    if 'leadPt-200-xx' in reg_name: label += ', high p_{T}^{lead}'
    if 'sublPt-00-20' in reg_name: label += ', low p_{T}^{subl}'
    if 'sublPt-20-50' in reg_name: label += ', medium p_{T}^{subl}'
    if 'sublPt-50-xx' in reg_name: label += ', high p_{T}^{subl}'

    return label


if __name__ == '__main__':
    main()
