################################################################################
# * Processes the plotHolder and write to file
################################################################################
import math
import random

# ROOT imports
import ROOT

# common includes
from MacroLogger import BaseLoggerClass
from pdb import set_trace as bp
from PlotHolder import GraphHolder
from GraphProcessor import GraphProcessor
from DKColor import DKColor


################################################################################
class PlotProcessor(BaseLoggerClass):
    """docstring for PlotProcessor"""
    def __init__(self, name, logLevel='WARNING'):
        BaseLoggerClass.__init__(self, name, logLevel)

        self.name = str(name)

    #_______________________________________
    def Process(self, plotHolder):
        self.logger.debug('Execute {}'.format(self.name))
        self.plotHolder = plotHolder

        self.plotHolder.type = self.plotHolder.Options('type')

        # ---------------------------------------------------------------------
        for gopts in self.plotHolder.Options('graphs'):
            gname = gopts['name']
            gtype = gopts['type']

            graphHolder = GraphHolder(gname, gtype)
            graphHolder.SetOptions(**gopts)

            if not self.plotHolder.Options('systematics') is None: 
                graphHolder.SetSystematics(self.plotHolder.Options('systematics'))

            gp = GraphProcessor(gname+'_Graphprocessor', self.logLevel)
            graphHolder = gp.Process(graphHolder)

            self.plotHolder.AddGraphHolder(graphHolder)
        # ---------------------------------------------------------------------

        if plotHolder.type == 'SimpleHist':
            self.SimpleHistProcessor()
        if plotHolder.type == 'RatioPlot':
            self.RatioPlotProcessor()
        if plotHolder.type == 'StackPlot':
            self.StackPlotProcessor()
        if plotHolder.type == 'RatioStackPlot':
            self.RatioStackPlotProcessor()
        
        self.plotHolder.canvas.Update()

        if self.plotHolder.Options('legend-opt')['draw']:
            self.LegendProcessor()

        if self.plotHolder.Options('text-box')['text']:
            self.TextBoxProcessor()
        
        plot_opt = self.plotHolder.Options('plot-opt')
        if plot_opt['log-x'] == True:
            self.logger.debug('set log x')
            self.plotHolder.canvas.SetLogx()
        if plot_opt['log-y'] == True:
            self.logger.debug('set log y')
            self.plotHolder.canvas.SetLogy()
        if plot_opt['log-z'] == True:
            self.logger.debug('set log z')
            self.plotHolder.canvas.SetLogz()

        return plotHolder

    #_______________________________________
    def SimpleHistProcessor(self):
        self.logger.debug("start SimpleHistProcessor")
        plot_opt = self.plotHolder.Options('plot-opt')
        self.plotHolder.canvas = ROOT.TCanvas(self.plotHolder.name,
                                              self.plotHolder.name,
                                              plot_opt['width'],
                                              plot_opt['height'])
        ROOT.SetOwnership(self.plotHolder.canvas, False)

        self.plotHolder.canvas.cd()
        if plot_opt['grid-x'] == True:
            self.plotHolder.canvas.SetGridx()
        if plot_opt['grid-y'] == True:
            self.plotHolder.canvas.SetGridy()

        first_graph = True
        for gName in self.plotHolder.GetGraphNames():
            graph = self.plotHolder.GetGraph(gName)
            graph_opt = self.plotHolder.GetGraphHolder(gName).Options('graph-opt')

            if (first_graph):
                graph = self.auto_y_scale(graph, graph_opt)
                first_graph = False
            graph.Draw(graph_opt['draw-opt'])
            if (graph_opt['xmaxFinal']!=None and graph_opt['xminFinal']!=None):
                graph.GetXaxis().SetRangeUser(graph_opt['xminFinal'], graph_opt['xmaxFinal'])

    #_______________________________________
    def StackPlotProcessor(self):
        self.logger.debug("start StackPlotProcessor")
        plot_opt = self.plotHolder.Options('plot-opt')
        self.plotHolder.canvas = ROOT.TCanvas(self.plotHolder.name,
                                               self.plotHolder.name,
                                               plot_opt['width'],
                                               plot_opt['height'])
        ROOT.SetOwnership(self.plotHolder.canvas, False)
        self.plotHolder.canvas.cd()

        if plot_opt['grid-x'] == True:
            self.plotHolder.canvas.SetGridx()
        if plot_opt['grid-y'] == True:
            self.plotHolder.canvas.SetGridy()

        hstack = ROOT.THStack(self.plotHolder.name, "")
        ROOT.SetOwnership(hstack, False)

        # first_graph = True
        graphNames = self.plotHolder.GetGraphNames()
        nostack_graphs = []
        for gName in graphNames:
            graph = self.plotHolder.GetGraph(gName)

            if not self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                hstack.Add(graph, "HIST")

        hall = hstack.GetStack().Last().Clone()
        ROOT.SetOwnership(hall, False)



        # //////////////////////////////////////////////////////////////////
        # SYSTEMATICS

        if (self.plotHolder.Options('systematics')):
            print "dkir START SYSTEMATICS CALCULATIONS"
            nbins = hall.GetNbinsX()
            sys = {} # {sys_name : [bin1_content, bin2_content, ...]}
            sys["nominal"] = [hall.GetBinContent(i) for i in range(1, nbins+1)]

            sysNames = self.plotHolder.Options('systematics')
            for sysName in sysNames:
                hstackSys = ROOT.THStack(self.plotHolder.name+sysName, "")
                for gName in graphNames:
                    if self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                        continue

                    graph = self.plotHolder.GetGraphSystematics(gName, sysName)
                    hstackSys.Add(graph, "HIST")


                hallSys = hstackSys.GetStack().Last().Clone()

                sys[sysName] = [hallSys.GetBinContent(i) for i in range(1, nbins+1)]

                hallSys.Delete()
                hstackSys.Delete()

            # split variations into up/down variations
            var_up = {}
            var_do = {}

            for sysName in sysNames:
                var = [sys[sysName][i] - sys["nominal"][i] for i in range(nbins)]
                # mirror onesided systematics
                if "__1up" in sysName:
                    sysNameDo = sysName.split("__")[0]+"__1down"
                    if not sysNameDo in sysNames: 
                        var_up[sysNameDo] = [max(-v,0) for v in var]
                        var_do[sysNameDo] = [min(-v,0) for v in var]

                var_up[sysName] = [max(v,0) for v in var]
                var_do[sysName] = [min(v,0) for v in var]

            # square sum all up and down variations
            uncert_up = [0]*nbins
            uncert_do = [0]*nbins
            for up in var_up.values(): 
                for i in range(nbins):
                    uncert_up[i] += up[i]**2
            for do in var_do.values():
                for i in range(nbins):
                    uncert_do[i] += do[i]**2

            # sqrt of summed variations
            uncert_up = [math.sqrt(u) for u in uncert_up]        
            uncert_do = [math.sqrt(u) for u in uncert_do]        

            stat_up = [hall.GetBinErrorUp(i) for i in range(1, nbins+1)]
            stat_do = [hall.GetBinErrorLow(i) for i in range(1, nbins+1)]

            tot_up = [math.sqrt(uncert_up[i]**2 + stat_up[i]**2) for i in range(nbins)]
            tot_do = [math.sqrt(uncert_do[i]**2 + stat_do[i]**2) for i in range(nbins)]

            for i in range(nbins): 
                t = max(tot_up[i], tot_do[i])
                hall.SetBinError(i+1, t)

        # //////////////////////////////////////////////////////////////////

        first_graph_name = graphNames[0]
        first_graph_opt = self.plotHolder.GetGraphHolder(first_graph_name).Options('graph-opt')

        # TODO: define draw-opt and graph-opt once for the stack and apply to all graphs
        # hstack = self.auto_y_scale(hstack, first_graph_opt)
        ymin, ymax = first_graph_opt['ymin'], first_graph_opt['ymax']
        if (ymax == None): 
            ymax = 1.4*(hall.GetMaximum()+hall.GetBinError(hall.GetMaximumBin()))
            for gName in graphNames:
                h = self.plotHolder.GetGraph(gName)
                ymax = max(ymax, h.GetMaximum())
        if (ymin == None): 
            ymin = 0

        hstack.SetMaximum(ymax)
        hstack.SetMinimum(ymin)
        hstack.Draw()

        if (first_graph_opt['xmin']!=None and first_graph_opt['xmax']!=None):
            hstack.GetXaxis().SetRangeUser(first_graph_opt['xmin'], first_graph_opt['xmax'])


        hall.SetMarkerSize(0)
        hall.SetFillColor(1)
        hall.SetFillStyle(3254)

        ex1 = ROOT.TExec("ex1", "gStyle->SetErrorX(0.5)")
        hall.GetListOfFunctions().Add(ex1)
        hall.Draw("SAMEE2")


        xtitle = first_graph_opt['x-title']
        ytitle = first_graph_opt['y-title']
        if xtitle: hstack.GetXaxis().SetTitle(xtitle)
        if ytitle: hstack.GetYaxis().SetTitle(ytitle)



        # plot "nostack" graphs
        for gName in graphNames:
            graph = self.plotHolder.GetGraph(gName)
            if self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                graph_opt = self.plotHolder.GetGraphHolder(gName).Options('graph-opt')
                graph.Draw(graph_opt['draw-opt'])


    #_______________________________________
    def RatioStackPlotProcessor(self):
        self.logger.debug("start RatioStackPlotProcessor")
        plot_opt = self.plotHolder.Options('plot-opt')
        self.plotHolder.canvas = ROOT.TCanvas(self.plotHolder.name,
                                              self.plotHolder.name,
                                              plot_opt['width'],
                                              plot_opt['height'])
        ROOT.SetOwnership(self.plotHolder.canvas, False)
        self.plotHolder.canvas.cd()

        pad1 = ROOT.TPad("pad1", "pad1", 0, 0.36, 1, 1.0)
        ROOT.SetOwnership(pad1, False)
        pad1.SetBottomMargin(0.03)
        if plot_opt['grid-x'] == True:
            pad1.SetGridx()
        if plot_opt['grid-y'] == True:
            pad1.SetGridy()
        if plot_opt['log-x'] == True:
            pad1.SetLogx()
        if plot_opt['log-y'] == True:
            pad1.SetLogy()
        pad1.Draw()

        pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, 0.36)
        ROOT.SetOwnership(pad2, False)
        pad2.SetTopMargin(0)
        if plot_opt['log-x'] == True:
            pad2.SetLogx()

        bottom_margin = plot_opt['bottom-margin']
        pad2.SetBottomMargin(bottom_margin)
        if plot_opt['grid-x'] == True:
            pad2.SetGridx()
        # if plot_opt['grid-y'] == True:
        pad2.SetGridy()
        pad2.Draw()

        ratio_dy = plot_opt['ratio-dy']
        ratio_ymin = plot_opt['ratio-ymin']
        ratio_ymax = plot_opt['ratio-ymax']

        pad1.cd()
        # //////////////////////////////////////////////////////////
        #  SAME AS StackPlotProcessor (TODO: re-structure, make this separate function)
        hstack = ROOT.THStack(self.plotHolder.name, "")
        ROOT.SetOwnership(hstack, False)

        # first_graph = True
        graphNames = self.plotHolder.GetGraphNames()
        nostack_graphs = []
        for gName in graphNames:
            graph = self.plotHolder.GetGraph(gName)

            if not self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                hstack.Add(graph, "HIST")

        hall = hstack.GetStack().Last().Clone()
        ROOT.SetOwnership(hall, False)

        hall_tot = hall.Clone()
        ROOT.SetOwnership(hall_tot, False)

        # /////////////////////////
        # SYSTEMATICS

        if (self.plotHolder.Options('systematics')):
            print "dkir START SYSTEMATICS CALCULATIONS"
            nbins = hall.GetNbinsX()
            sys = {} # {sys_name : [bin1_content, bin2_content, ...]}
            sys["nominal"] = [hall.GetBinContent(i) for i in range(1, nbins+1)]

            sysNames = self.plotHolder.Options('systematics')
            for sysName in sysNames:
                hstackSys = ROOT.THStack(self.plotHolder.name+sysName, "")
                for gName in graphNames:
                    if self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                        continue

                    graph = self.plotHolder.GetGraphSystematics(gName, sysName)
                    hstackSys.Add(graph, "HIST")


                hallSys = hstackSys.GetStack().Last().Clone()

                sys[sysName] = [hallSys.GetBinContent(i) for i in range(1, nbins+1)]

                hallSys.Delete()
                hstackSys.Delete()

            # split variations into up/down variations
            var_up = {}
            var_do = {}

            for sysName in sysNames:
                var = [sys[sysName][i] - sys["nominal"][i] for i in range(nbins)]
                # mirror onesided systematics
                if "__1up" in sysName:
                    sysNameDo = sysName.split("__")[0]+"__1down"
                    if not sysNameDo in sysNames: 
                        var_up[sysNameDo] = [max(-v,0) for v in var]
                        var_do[sysNameDo] = [min(-v,0) for v in var]

                var_up[sysName] = [max(v,0) for v in var]
                var_do[sysName] = [min(v,0) for v in var]

            # square sum all up and down variations
            uncert_up = [0]*nbins
            uncert_do = [0]*nbins
            for up in var_up.values(): 
                for i in range(nbins):
                    uncert_up[i] += up[i]**2
            for do in var_do.values():
                for i in range(nbins):
                    uncert_do[i] += do[i]**2

            # sqrt of summed variations
            uncert_up = [math.sqrt(u) for u in uncert_up]        
            uncert_do = [math.sqrt(u) for u in uncert_do]        

            stat_up = [hall.GetBinErrorUp(i) for i in range(1, nbins+1)]
            stat_do = [hall.GetBinErrorLow(i) for i in range(1, nbins+1)]

            tot_up = [math.sqrt(uncert_up[i]**2 + stat_up[i]**2) for i in range(nbins)]
            tot_do = [math.sqrt(uncert_do[i]**2 + stat_do[i]**2) for i in range(nbins)]

            for i in range(nbins): 
                t = max(tot_up[i], tot_do[i])
                hall_tot.SetBinError(i+1, t)

        # /////////////////////// end of systematics

        first_graph_name = graphNames[0]
        first_graph_opt = self.plotHolder.GetGraphHolder(first_graph_name).Options('graph-opt')

        ymin, ymax = first_graph_opt['ymin'], first_graph_opt['ymax']
        if (ymax == None): 
            ymax = 1.4*(hall_tot.GetMaximum()+hall_tot.GetBinError(hall_tot.GetMaximumBin()))
            for gName in graphNames:
                h = self.plotHolder.GetGraph(gName)
                ymax = max(ymax, h.GetMaximum())
        if (ymin == None): 
            ymin = 0

        hstack.SetMaximum(ymax)
        hstack.SetMinimum(ymin)
        hstack.Draw()

        if (first_graph_opt['xmin']!=None and first_graph_opt['xmax']!=None):
            hstack.GetXaxis().SetRangeUser(first_graph_opt['xmin'], first_graph_opt['xmax'])



        hall_tot.SetMarkerSize(0)
        hall_tot.SetFillColor(1)
        hall_tot.SetFillStyle(3254)

        ex1 = ROOT.TExec("ex1", "gStyle->SetErrorX(0.5)")
        hall_tot.GetListOfFunctions().Add(ex1)
        hall_tot.Draw("SAMEE2")


        xtitle = first_graph_opt['x-title']
        ytitle = first_graph_opt['y-title']
        if xtitle: hstack.GetXaxis().SetTitle(xtitle)
        if ytitle: hstack.GetYaxis().SetTitle(ytitle)



        data = None
        data_opt = None
        # plot "nostack" graphs
        for gName in graphNames:
            graph = self.plotHolder.GetGraph(gName)
            if self.plotHolder.GetGraphHolder(gName).Options('nostack'):
                graph_opt = self.plotHolder.GetGraphHolder(gName).Options('graph-opt')
                graph.Draw(graph_opt['draw-opt'])
                if ('data_' in graph.GetName()):
                    data = graph.Clone()
                    ROOT.SetOwnership(data, False)
                    data_opt = graph_opt



        # ////////////////////////////////////////////////////////// end of StackPlotProcessor

        pad2.cd() 

        hall_tot_ratio = hall_tot.Clone()
        ROOT.SetOwnership(hall_tot_ratio, False)
        hall_tot_ratio.Divide(hall)

        hall_tot_ratio.GetYaxis().SetRangeUser(max(0.001, 1-ratio_dy), 1+ratio_dy)

        hall_tot_ratio.Draw("E2")

        data.Divide(hall)
        # if (ratio_ymin and ratio_ymax):
        #     data.GetYaxis().SetRangeUser(ratio_ymin, ratio_ymax)
        # else:
        # data.GetYaxis().SetRangeUser(max(0.001, 1-ratio_dy), 1+ratio_dy)
        # data.Draw(data_opt['draw-opt'])
        data.Draw("SAMEE0X0")




        hall_tot_ratio.GetXaxis().SetLabelSize(0.10)
        hall_tot_ratio.GetXaxis().SetTitleSize(0.10)
        hall_tot_ratio.GetXaxis().SetTitleOffset(1.4)
        hall_tot_ratio.GetXaxis().SetLabelOffset(0.02)

        hall_tot_ratio.GetYaxis().SetNdivisions(5)
        hall_tot_ratio.GetYaxis().SetLabelSize(0.10)
        hall_tot_ratio.GetYaxis().SetLabelOffset(0.006)
        hall_tot_ratio.GetYaxis().SetTitleSize(0.10)
        hall_tot_ratio.GetYaxis().SetTitleOffset(0.72)
        hall_tot_ratio.GetYaxis().SetTitle("Data / Pred.")

        hstack.GetXaxis().SetLabelSize(0)
        hstack.GetYaxis().SetLabelSize(0.06)
        hstack.GetYaxis().SetTitleSize(0.06)
        hstack.GetYaxis().SetTitleOffset(1.15)
        hstack.GetYaxis().SetLabelOffset(0.005)


        pad2.Draw()
        # test.Draw()
        # pad1.RedrawAxis()
        # pad2.RedrawAxis()
        # pad1.RedrawAxis("G")
        # pad2.RedrawAxis("G")





    #_______________________________________
    def auto_y_scale(self, graph, graph_opt):
        ymin, ymax = graph_opt['ymin'], graph_opt['ymax']

        # needed for e.g. TreeToProfile (when initial histogram has different binning then final graph)
        ymin = graph_opt['ymin']
        ymax = graph_opt['ymax']
        if (ymin != None): ymin = ymin
        if (ymax != None): ymax = ymax

        if (ymin != None and ymax != None):
            if (type(graph) == ROOT.THStack):
                graph.SetMinimum(ymin)
                graph.SetMaximum(ymax)
            else:
                graph.GetYaxis().SetRangeUser(ymin, ymax)
            return graph

        for gName in self.plotHolder.GetGraphNames():
            h = self.plotHolder.GetGraph(gName)

            if type(h) in [ROOT.TGraph, ROOT.TGraphErrors, ROOT.TGraphAsymmErrors]:
                new_ymin = h.GetHistogram().GetMinimum()
                new_ymax = h.GetHistogram().GetMaximum()
            elif type(h) in [ROOT.TH2, ROOT.TH2F, ROOT.TH2D]:
                new_ymin = h.GetYaxis().GetXmin()
                new_ymax = h.GetYaxis().GetXmax()
            else:
                new_ymin = h.GetMinimum()
                new_ymax = h.GetMaximum()

            if (ymin == None or ymin > new_ymin):
                ymin = new_ymin
            if (ymax == None or ymax < new_ymax):
                ymax = new_ymax

        self.logger.debug("y axis... SetRangeUser({}, {})".format(ymin, ymax))
        if (type(graph) == ROOT.THStack):
            graph.SetMinimum(ymin)
            graph.SetMaximum(ymax)
        else:
            graph.GetYaxis().SetRangeUser(ymin, ymax)

        return graph

    #_______________________________________
    def RatioPlotProcessor(self):
        self.logger.debug("start RatioPlotProcessor")
        plot_opt = self.plotHolder.Options('plot-opt')
        self.plotHolder.canvas = ROOT.TCanvas(self.plotHolder.name,
                                              self.plotHolder.name,
                                              plot_opt['width'],
                                              plot_opt['height'])
        ROOT.SetOwnership(self.plotHolder.canvas, False)
        self.plotHolder.canvas.cd()

        pad1 = ROOT.TPad("pad1", "pad1", 0, 0.35, 1, 1.0)
        ROOT.SetOwnership(pad1, False)
        pad1.SetBottomMargin(0.03)
        if plot_opt['grid-x'] == True:
            pad1.SetGridx()
        if plot_opt['grid-y'] == True:
            pad1.SetGridy()
        if plot_opt['log-x'] == True:
            pad1.SetLogx()
        if plot_opt['log-y'] == True:
            pad1.SetLogy()
        pad1.Draw()

        pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, 0.35)
        ROOT.SetOwnership(pad2, False)
        pad2.SetTopMargin(0)
        if plot_opt['log-x'] == True:
            pad2.SetLogx()

        bottom_margin = plot_opt['bottom-margin']
        pad2.SetBottomMargin(bottom_margin)
        if plot_opt['grid-x'] == True:
            pad2.SetGridx()
        pad2.SetGridy()
        pad2.Draw()

        ratio_dy = plot_opt['ratio-dy']
        ratio_ymin = plot_opt['ratio-ymin']
        ratio_ymax = plot_opt['ratio-ymax']
        first_graph = True
        for gName in self.plotHolder.GetGraphNames():
            graph = self.plotHolder.GetGraph(gName)
            graph_opt = self.plotHolder.GetGraphHolder(gName).Options('graph-opt')

            pad1.cd()
            if (first_graph):
                graph = self.auto_y_scale(graph, graph_opt)
                first_graph = False
            graph.Draw(graph_opt['draw-opt'])

            pad2.cd() 
            compared_graph_name = self.plotHolder.GetGraphNames()[0]
            compared_graph = self.plotHolder.GetGraph(compared_graph_name)

            ratio = graph.Clone()
            ROOT.SetOwnership(ratio, False)

            ratio.Divide(compared_graph)
            if (ratio_ymin and ratio_ymax):
                ratio.GetYaxis().SetRangeUser(ratio_ymin, ratio_ymax)
            else:
                ratio.GetYaxis().SetRangeUser(max(0, 1-ratio_dy), 1+ratio_dy)
            ratio.Draw(graph_opt['draw-opt'])

            ratio.GetXaxis().SetLabelSize(0.11)
            ratio.GetXaxis().SetTitleSize(0.11)
            ratio.GetXaxis().SetTitleOffset(1.2)
            ratio.GetXaxis().SetLabelOffset(0.03)

            ratio.GetYaxis().SetNdivisions(5)
            ratio.GetYaxis().SetLabelSize(0.11)
            ratio.GetYaxis().SetLabelOffset(0.018)
            ratio.GetYaxis().SetTitleSize(0.11)
            ratio.GetYaxis().SetTitleOffset(0.78)
            ratio.GetYaxis().SetTitle("Ratio")

            graph.GetXaxis().SetLabelSize(0)
            graph.GetYaxis().SetLabelSize(0.06)
            graph.GetYaxis().SetTitleSize(0.06)
            graph.GetYaxis().SetLabelOffset(0.015)

        pad1.RedrawAxis()
        pad2.RedrawAxis()
        pad1.RedrawAxis("G")
        pad2.RedrawAxis("G")


    #_______________________________________
    def TextBoxProcessor(self):
        self.plotHolder.canvas.cd()
        c = self.plotHolder.canvas

        text_opt = self.plotHolder.Options('text-box')
        xmin = c.GetUxmin()
        xmax = c.GetUxmax()
        ymin = c.GetUymin()
        ymax = c.GetUymax()

        x = text_opt['x']
        y = text_opt['y']
        text = text_opt['text']
        size = text_opt['size']

        if isinstance(text, str):
            text = [text]
            # y = [y]

        for i in range(len(text)): 
            box = ROOT.TLatex(x, y, text[i])
            box.SetNDC()
            y = y - text_opt['dy']
            box.SetTextSize(size)
            ROOT.SetOwnership(box, False)
            box.SetNDC()
            box.Draw()

    #_______________________________________
    def LegendProcessor(self):
        self.plotHolder.canvas.cd()

        leg_opt = self.plotHolder.Options('legend-opt')

        x1 = leg_opt['x1']
        y1 = leg_opt['y1']
        x2 = leg_opt['x2']
        y2 = leg_opt['y2']

        n_leg_entries = 0
        graph_names = self.plotHolder.GetGraphNames() 
        if (leg_opt['reverse']):
            graph_names.reverse()

        for gName in graph_names:
            marker = self.plotHolder.GetGraphHolder(gName).Options('leg-marker')
            if marker is not '': n_leg_entries += 1

        if (leg_opt["addUncert"]):
            n_leg_entries += 1

        if (leg_opt['dy']):
            y1 = y2 - leg_opt['dy']*n_leg_entries

        leg = ROOT.TLegend(x1, y1, x2, y2)
        leg.SetFillStyle(0)
        leg.SetLineWidth(0)

        ROOT.SetOwnership(leg, False)

        for gName in graph_names:
            marker = self.plotHolder.GetGraphHolder(gName).Options('leg-marker')
            if marker is not '':
                leg.AddEntry(self.plotHolder.GetGraph(gName), gName, marker)

        if leg_opt["addUncert"]: 
            h  = ROOT.TH1F(str(random.random()), "", 10, 0, 1)
            ROOT.SetOwnership(h, False)
            h.SetMarkerSize(0)
            h.SetLineWidth(0)
            h.SetFillColor(1)
            h.SetFillStyle(3254)
            leg.AddEntry(h, "Uncertainty", "f")


        leg.Draw(leg_opt['option'])

