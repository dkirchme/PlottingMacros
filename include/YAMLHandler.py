################################################################################
# * Class the load and read yaml files
################################################################################

# python includes
import sys
import yaml

# common includes
from MacroLogger import *


################################################################################
class YAMLHandler(BaseLoggerClass):
    #_______________________________________
    def __init__(self, name="YAMLHandler", logLevel="WARNING"):
        BaseLoggerClass.__init__(self, name, logLevel)

    #_______________________________________
    def ReadYAML(self, yaml_file):
        try:
            self.logger.debug("Try open yaml file %s" % yaml_file)
            configFile = open(yaml_file, "r")
            self.logger.debug("Successfully open file %s" % yaml_file)

            self.logger.debug("Try to load yaml file %s" % yaml_file)
            config = yaml.load(configFile)
            self.logger.debug("Successfully open file %s" % yaml_file)

            configFile.close()

            return config

        except IOError as err:
            self.logger.error("IOError for %s" % yaml_file)
            self.logger.error("IOError: %s" % err.strerror)
            raise

        except yaml.YAMLError as err:
            self.logger.error("Failure during the loading of yaml file '%s'" % yaml_file)
            self.logger.error(err)
            raise

        except:
            self.logger.error("Unexpected error for %s" % yaml_file)
            self.logger.error("Unexpected error: %r : %r" % (sys.exc_info()[0], sys.exc_info()[1]))
