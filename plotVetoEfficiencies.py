#!/usr/bin/python

import ROOT
import os
import sys
# from DKColor import getWhiteToBlue

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# file = "/ZIH.fast/users/kirchmeier/Run/run10/data-DTNT/user.dkirchme.mc15_13TeV.303366.RS_G_hh_bbtt_hh_c10_M1800.recon.AOD.e4438_s2608_r6869_v02.root"
file = "/home/s3608936/Code/DiHiggsAnalysisR21/run/DiHiggsPlots/submitDir_v9_Rv38/Merged.root"

f = ROOT.TFile(file)
# t = f.Get("tree")

# ROOT.gStyle.SetPaintTextFormat(".3f")
# n_colors, palette = getWhiteToBlue()
# ROOT.gStyle.SetPalette(n_colors, palette)


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
signal_masses = [1000, 1200, 1400, 1600, 1800, 2000, 2500, 3000]
signal_samples = ["Xtohh{}_Hw".format(m) for m in signal_masses]

regions = ["2tag2pjet_0ptv_SR_Additional1btagFJ", "2tag2pjet_0ptv_SR_Additional2btagFJ", "2tag2pjet_0ptv_SR_SelFJVRJetOverlap", "1tag2pjet_0ptv_SR_Additional1btagFJ", "1tag2pjet_0ptv_SR_Additional2btagFJ", "1tag2pjet_0ptv_SR_SelFJVRJetOverlap"]
# c = ROOT.TCanvas()

    # heff = ROOT.TGraph()
        # heff.SetPoint(i, i, Npass/Ntot)
hmap = {}
for j in range(len(regions)):
    heff = ROOT.TH1F(regions[j], regions[j], len(signal_samples), 0, len(signal_samples))
    hmap[regions[j]] = heff

    for i in range(len(signal_samples)):
        heff.GetXaxis().SetBinLabel(i+1, "M = {} GeV".format(signal_masses[i]))
        print signal_samples[i]
        h = f.Get("{}_{}".format(signal_samples[i], regions[j]))
        Ntot = h.Integral()
        print Ntot
        Npass = h.GetBinContent(1)
        print Npass
        heff.SetBinContent(i+1, Npass/Ntot)


c = []
for region in regions:
    c.append(ROOT.TCanvas())
    c[-1].cd()
    hmap[region].GetYaxis().SetRangeUser(0.9, 1.01)
    hmap[region].GetYaxis().SetTitle("Signal Efficiency")

    hmap[region].Draw("PL")
    c[-1].SaveAs("output/vetoStudies/{}.pdf".format(region))

# c = ROOT.TCanvas()
# heff.Draw()
# h = f.Get("Xtohh1000_Hw_2tag2pjet_0ptv_SR_Additional1btagFJ")


# t.Draw("truth_n_tracks_subl:n_tracks_subl>>hreco(6,0,6,5,0,5)", "truth_matched")
# t.Draw("truth_n_tracks_lead:n_tracks_lead>>+hreco", "truth_matched")


# t.Draw("truth_n_tracks_subl>>htruth1", "truth_n_tracks_subl==1")
# t.Draw("truth_n_tracks_lead>>+htruth1", "truth_n_tracks_lead==1")
# t.Draw("truth_n_tracks_subl>>htruth3", "truth_n_tracks_subl==3")
# t.Draw("truth_n_tracks_lead>>+htruth3", "truth_n_tracks_lead==3")

# ntruth1 = ROOT.gDirectory.Get("htruth1").GetEntries()
# ntruth3 = ROOT.gDirectory.Get("htruth3").GetEntries()

# hreco = ROOT.gDirectory.Get("hreco")

# h = ROOT.TH2F("h", "", 2, 0, 1, 2, 0, 1)
# xaxis = h.GetXaxis()
# yaxis = h.GetYaxis()
# xaxis.SetBinLabel(1, "reco 1p")
# xaxis.SetBinLabel(2, "reco mp")
# yaxis.SetBinLabel(1, "true 1p")
# yaxis.SetBinLabel(2, "true mp")

# n = ntruth1+ntruth3
# h.SetBinContent(1, 1, hreco.Integral(2,2, 2,2) / n )  # true 1p reco 1p
# h.SetBinContent(2, 1, hreco.Integral(3,4, 2,2) / n )  # true 1p reco mp
# h.SetBinContent(1, 2, hreco.Integral(2,2, 4,4) / n )  # true mp reco 1p
# h.SetBinContent(2, 2, hreco.Integral(3,4, 4,4) / n )  # true mp reco mp

# h.Draw("coltext")
# c.Update()
# c.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_fractions.png")

# # # ----------------------------------------------------------------------------
# # # ----------------------------------------------------------------------------




# ce = ROOT.TCanvas()
# t.Draw("truth_n_tracks_subl:n_tracks_subl>>hpas(6,0,6,5,0,5)", "truth_matched")
# t.Draw("truth_n_tracks_lead:n_tracks_lead>>+hpas", "truth_matched")

# t.Draw("truth_n_tracks_subl:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# for i in range(10):
#    t.Draw("truth_n_tracks_subl:{}>>+htot".format(i), "truth_matched", "COLTEXT")
#    t.Draw("truth_n_tracks_lead:{}>>+htot".format(i), "truth_matched", "COLTEXT")

# hpas = ROOT.gDirectory.Get("hpas")
# htot = ROOT.gDirectory.Get("htot")

# hpas.Divide(htot)
# hpas.GetYaxis().SetTitle("true n_{tracks}")
# hpas.GetXaxis().SetTitle("reco n_{tracks}")

# hpas.Draw("COLTEXT")
# ce.Update()
# ce.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency.png")

# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------


# c2 = ROOT.TCanvas()
# t.Draw("truth_n_tracks_subl:n_tracks_subl>>hpas(6,0,6,5,0,5)", "truth_matched")

# t.Draw("truth_n_tracks_subl:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# for i in range(10):
#    t.Draw("truth_n_tracks_subl:{}>>+htot".format(i), "truth_matched", "COLTEXT")

# hpas = ROOT.gDirectory.Get("hpas")
# htot = ROOT.gDirectory.Get("htot")

# hpas.Divide(htot)
# hpas.GetYaxis().SetTitle("true n_{tracks}^{subleading}")
# hpas.GetXaxis().SetTitle("reco n_{tracks}^{subleading}")

# hpas.Draw("COLTEXT")
# c2.Update()
# c2.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency_subl.png")


# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------


# c3 = ROOT.TCanvas()
# t.Draw("truth_n_tracks_lead:n_tracks_lead>>hpas(6,0,6,5,0,5)", "truth_matched")

# t.Draw("truth_n_tracks_lead:-1>>htot(6,0,6,5,0,5)", "truth_matched", "COLTEXT")
# for i in range(10):
#    t.Draw("truth_n_tracks_lead:{}>>+htot".format(i), "truth_matched", "COLTEXT")

# hpas = ROOT.gDirectory.Get("hpas")
# htot = ROOT.gDirectory.Get("htot")

# hpas.Divide(htot)
# hpas.GetYaxis().SetTitle("true n_{tracks}^{leading}")
# hpas.GetXaxis().SetTitle("reco n_{tracks}^{leading}")

# hpas.Draw("COLTEXT")
# c3.Update()
# c3.SaveAs("~/DiTauRecValidation/PlottingMacros/output/trackEff/track_reco_efficiency_lead.png")


