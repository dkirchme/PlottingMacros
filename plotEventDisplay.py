#!/usr/bin/python

import ROOT
import os
import sys

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

f = ROOT.TFile('/raid1/users/kirchmeier/BoostedAnalysis/BoostedAnalysis/run/latest/hist-user.dkirchme.mc15_13TeV.425100.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root')
# f = ROOT.TFile('/raid1/users/kirchmeier/BoostedAnalysis/BoostedAnalysis/run/output_303366.DAOD_HIGG4D3_20170518_19-12-57/hist-user.dkirchme.mc15_13TeV.303366.DAOD_HIGG4D3.e4438_s2608_r6869_v02_EXT0.root')
# f = ROOT.TFile('/ZIH.fast/users/kirchmeier/NTuples10/output_425104.DAOD_HIGG4D3_20170518_23-39-24/hist-user.dkirchme.mc15_13TeV.425104.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_v3_EXT0.root')

t = f.Get('Nominal/BaselineSelection_tree_APPLY_DITAU_BASESELECTION(kDiTaus,0)') 

cs = []

count = 0
for e in t:
    count+=1
    # if len(e.ditau_pt)<0:
    #     continue

    if count==300:
        break



    # if (count < 6755 or count > 6775):
    #     continue
    # if (count != 6765 or count > 6775):
    #     continue

    if len(e.ditau_pt)==0:
        continue

    if not (e.ditau_truth_deltaR[0]<0.2 and e.ditau_truthHadHad[0]):
        continue
    else:
        print count, e.ditau_truth_deltaR[0]
    
    cs.append(ROOT.TCanvas())
    # ROOT.SetOwnership(cs[-1], False)

    g_dummy = ROOT.TGraph()
    ROOT.SetOwnership(g_dummy, False)
    g_dummy.SetPoint(100, 2.7, 3.14)
    g_dummy.SetPoint(200, -2.7, 3.14)
    g_dummy.SetPoint(300, 2.7, -3.14)
    g_dummy.SetPoint(400, -2.7, -3.14)

    g_true_ditau_jet = ROOT.TGraph()
    ROOT.SetOwnership(g_true_ditau_jet, False)
    g_true_ditau_taus = ROOT.TGraph()
    ROOT.SetOwnership(g_true_ditau_taus, False)
    g_ditau_jet = ROOT.TGraph()
    ROOT.SetOwnership(g_ditau_jet, False)

    g_ditau_subjet = ROOT.TGraph()
    ROOT.SetOwnership(g_ditau_subjet, False)
    g_ditau_jet_truthmatched = ROOT.TGraph()
    ROOT.SetOwnership(g_ditau_jet_truthmatched, False)

    for i in range(len(e.true_ditau_pt)):
        g_true_ditau_jet.SetPoint(g_true_ditau_jet.GetN(), e.true_ditau_eta[i], e.true_ditau_phi[i])
        g_true_ditau_taus.SetPoint(g_true_ditau_taus.GetN(), e.true_ditau_lead_eta[i], e.true_ditau_lead_phi[i])
        g_true_ditau_taus.SetPoint(g_true_ditau_taus.GetN(), e.true_ditau_subl_eta[i], e.true_ditau_subl_phi[i])
    for i in range(len(e.ditau_seedjet_pt)):
        g_ditau_jet.SetPoint(g_ditau_jet.GetN(), e.ditau_seedjet_eta[i], e.ditau_seedjet_phi[i])
        g_ditau_subjet.SetPoint(g_ditau_subjet.GetN(), e.ditau_sjlead_eta[i], e.ditau_sjlead_phi[i])
        g_ditau_subjet.SetPoint(g_ditau_subjet.GetN(), e.ditau_sjsubl_eta[i], e.ditau_sjsubl_phi[i])
        if (e.ditau_truthHadHad[i]): 
            g_ditau_jet_truthmatched.SetPoint(g_ditau_jet_truthmatched.GetN(), e.ditau_seedjet_eta[i], e.ditau_seedjet_phi[i])


    g_dummy.SetMarkerColor(ROOT.kWhite)
    g_dummy.Draw('AP')

    g_true_ditau_jet.SetMarkerSize(4)
    if (g_true_ditau_jet.GetN()>0):
        g_true_ditau_jet.Draw('PSAME')

    g_ditau_jet.SetMarkerSize(3)
    g_ditau_jet.SetMarkerColor(ROOT.kRed)
    if (g_ditau_jet.GetN()>0):
        g_ditau_jet.Draw('PSAME')

    g_ditau_jet_truthmatched.SetMarkerSize(2.5)
    g_ditau_jet_truthmatched.SetMarkerColor(ROOT.kBlue)
    if (g_ditau_jet_truthmatched.GetN()>0):
        g_ditau_jet_truthmatched.Draw('PSAME')


    g_true_ditau_taus.SetMarkerSize(2)
    g_true_ditau_taus.SetMarkerColor(ROOT.kGray)
    if (g_true_ditau_taus.GetN()>0):
        g_true_ditau_taus.Draw('PSAME')

    g_ditau_subjet.SetMarkerColor(ROOT.kRed+3)
    if (g_ditau_subjet.GetN()>0):
        g_ditau_subjet.Draw('PSAME')


