################################################################################
# * Merges custom configuration with default configuration
################################################################################

# common imports
from MacroLogger import BaseLoggerClass
from YAMLHandler import YAMLHandler
# from EmbeddedIPython import breakpoint as bp


#_______________________________________
def isIterable(config):
    return (type(config) in (dict, list))


#_______________________________________
def isList(config):
    return (type(config) is list)


#_______________________________________
def isDict(config):
    return (type(config) is dict)


################################################################################
class ConfigHandler(BaseLoggerClass, dict):
    """docstring for ConfigHandler"""
    def __init__(self, name, logLevel='WARNING'):
        BaseLoggerClass.__init__(self, name, logLevel)

        self.name = name
        self.cconfig = None  # custom configuration
        self.dconfig = None  # default configuration
        self.mergedConfigs = False

    #_______________________________________
    # returns the config dictionary
    def GetConfig(self):
        if self.mergedConfigs is False:
            self.logger.error('custom and default configs are not merged yet')
            return None
        return self.cconfig

    #_______________________________________
    # returns iterable configs
    def GetConfigs(self):
        if self.mergedConfigs is False:
            self.logger.error('custom and default configs are not merged yet')
            return None
        return self.cconfig.iteritems()

    #_______________________________________
    def GetNConfigs(self):
        return len(self.cconfig)
    #_______________________________________
    def SetConfigs(self, default, custom):
        self.logger.debug('SetConfig')
        self.dconfig = default
        self.cconfig = custom

        self.CombineConfigs()

    #_______________________________________
    def SetConfigsByYaml(self, default, custom):
        self.logger.debug('SetConfig')
        self.dconfig = self.GetConfigFromYaml(default)
        self.cconfig = self.GetConfigFromYaml(custom)

        # ignore anchors
        if ('ANCHORS' in self.dconfig): 
            self.dconfig.pop('ANCHORS')
        if ('ANCHORS' in self.cconfig): 
            self.cconfig.pop('ANCHORS')

        # only plot those plots that are defined in PLOTS
        if ('PLOTS' in self.cconfig):
            plot_list = self.cconfig['PLOTS']
            self.cconfig.pop('PLOTS')
            if (plot_list):
                self.cconfig = {k:v for (k,v) in self.cconfig.items() if k in plot_list}

        self.CombineConfigs()

    #_______________________________________
    def GetConfigFromYaml(self, yaml_file):
        self.logger.debug('GetConfigFromYaml')
        yH = YAMLHandler()
        self.logger.debug("Open yaml file {}".format(yaml_file))
        try:
            config = yH.ReadYAML(yaml_file)
        except:
            self.logger.error("Failure during loading of file {}, no options set.".format(yaml_file))
            raise

        return config

    #_______________________________________
    def CombineConfigs(self):
        self.logger.debug('CombineConfigs')
        for cPlotName in self.cconfig:
            for dPlotName in self.dconfig:
                dType = self.dconfig[dPlotName]['type']
                cType = self.cconfig[cPlotName]['type']
                self.logger.debug('d: {} c: {}'.format(dType, cType))
                if dType == cType:
                    self.cconfig[cPlotName] = self.Merge(self.dconfig[dPlotName],
                                                         self.cconfig[cPlotName])

        self.mergedConfigs = True
        pass

    #_______________________________________
    def Merge(self, dconfig, cconfig):
        self.logger.debug('merge')
        self.logger.debug(dconfig)
        self.logger.debug(cconfig)

        # Check custom config for unexpected configurations
        for key in cconfig:
            if not key in dconfig:
                self.logger.warning('key "{}" is not known in default config.'.format(key))

        # Do the merging
        for key in dconfig:
            self.logger.debug('test key "{}"'.format(key))
            if key in cconfig:

                # value seems to be another dict
                if isDict(dconfig[key]):
                    cconfig[key] = self.Merge(dconfig[key], cconfig[key])

                # Continue if the value is not iterable:
                elif not isIterable(dconfig[key]):
                    continue

                # If value is a list, find out with which default has to be compared
                elif isList(dconfig[key]):
                    # find the right typed list item for each list item of custom config
                    for i in range(len(cconfig[key])):
                        # x is the index of the default list item which will be merged
                        x = 0
                        if 'type' in cconfig[key][i]:
                            # check if type is available in default config
                            if (not cconfig[key][i]['type'] in [c['type'] for c in dconfig[key]]):
                                self.logger.error("type {} not found in default config".format(cconfig[key][i]['type']))
                                return None

                            for j in range(len(dconfig[key])):
                                if dconfig[key][j]['type'] == cconfig[key][i]['type']:
                                    x = j
                                    break

                        if not isIterable(dconfig[key][x]):
                            continue

                        cconfig[key][i] = self.Merge(dconfig[key][x], cconfig[key][i])

                else:
                    self.logger.warning('unexpected key "{} in {}"'.format(key, cconfig))

            else:
                self.logger.debug('no key "{}" in custom conf. Set to default.'.format(key))
                cconfig[key] = dconfig[key]

        # self.logger.debug('return: {}'.format(cconfig))
        return cconfig
