################################################################################
# * Main class for user to access the plotter tools
################################################################################

# common includes
from MacroLogger import BaseLoggerClass
from OptionHandler import BaseOptionClass
from PlotHolder import PlotHolder
from PlotHolder import GraphHolder
# from EmbeddedIPython import breakpoint as bp
from GraphProcessor import GraphProcessor
from PlotProcessor import PlotProcessor
# from AtlasStyle import AtlasStyle

import copy

################################################################################
class MainPlotter(BaseLoggerClass, BaseOptionClass):
    """docstring for MainPlotter"""
    def __init__(self, name, logLevel="WARNING"):
        BaseLoggerClass.__init__(self, name, logLevel)
        BaseOptionClass.__init__(self, name, logLevel)

        self.name = name

    #_______________________________________
    def Plot(self, f_out, path_out, file_formats, save_all_hists):
        self.logger.debug('prepare plot {}'.format(self.name))

        # setup plot holder
        plotHolder = PlotHolder(self.name, logLevel=self.logLevel)

        plotHolder.SetOptions(**self.Options())

        pp = PlotProcessor(self.name+'_PlotProcessor', self.logLevel)
        plotHolder = pp.Process(plotHolder)

        f_out.cd()
        plotHolder.canvas.Write()

        # save all graphs to output root file
        if save_all_hists:
            for gName in plotHolder.GetGraphNames():
                graph = plotHolder.GetGraph(gName)
                graph.SetLineColor(1)
                graph.SetMarkerColor(1)
                graph.Write(self.name+"_"+gName)

        known_formats = ['pdf', 'png', 'ps', 'eps', 'svg', 'tex', 'gif', 'tiff', 'cxx', 'xml', 'root']
        if file_formats:
            file_formats = file_formats.split(',')
            for  file_format in file_formats:
                if file_format not in known_formats:
                    self.logger.warning('Given file format \'{}\' not known'.format(file_format))
                plotHolder.canvas.Print(path_out+self.name+"."+file_format)

