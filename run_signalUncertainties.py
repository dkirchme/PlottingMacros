import ROOT

input_file_path = "/scratch/ws/s3608936-DiHiggsAnalysis/run/DiHiggsPlots/submitDir_v10_Rv66/Merged.root"

f = ROOT.TFile(input_file_path, "READ")

sig_py = ["Xtohh1000", "Xtohh2000"]
sig_hw = ["Xtohh1000_Hw", "Xtohh2000_Hw"]
region = "_2tag2pjet_0ptv_MwindowDphiGt1_subsmhh"

h1000_py = f.Get(sig_py[0]+region)
h1000_hw = f.Get(sig_hw[0]+region)
h2000_py = f.Get(sig_py[1]+region)
h2000_hw = f.Get(sig_hw[1]+region)


u1 = (h1000_py.Integral() - h1000_hw.Integral()) / h1000_hw.Integral()
u2 = (h2000_py.Integral() - h2000_hw.Integral()) / h2000_hw.Integral()
print "Herwig7 M=1000:", h1000_hw.Integral()
print "Pythia8 M=1000:", h1000_py.Integral(), "(", (h1000_py.Integral() - h1000_hw.Integral()) / h1000_hw.Integral() * 100, "% )"
print "Herwig7 M=2000:", h2000_hw.Integral()
print "Pythia8 M=2000:", h2000_py.Integral(), "(", (h2000_py.Integral() - h2000_hw.Integral()) / h2000_hw.Integral() * 100, "% )"

print "uncert = ", (u2 - u1)*100, "% * M [TeV] ", (u1 - (u2-u1))*100, "%"



# Herwig7 M=1000: 0.303545760897
# Pythia8 M=1000: 0.298909435514 ( -1.52738927027 % )
# Herwig7 M=2000: 1.69406019419
# Pythia8 M=2000: 1.83624840321 ( 8.39333864932 % )
# uncert =  6.86594937905 % * M [TeV]  -5.33856010877 %

