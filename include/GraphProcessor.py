################################################################################
# * Processes the GraphHolder
################################################################################

# python imports
import random
import glob
from array import array

# ROOT imports
import ROOT

# common includes
from MacroLogger import BaseLoggerClass
from pdb import set_trace as bp
from DKColor import DKColor


################################################################################
class GraphProcessor(BaseLoggerClass):
    """docstring for GraphProcessor"""
    def __init__(self, name, logLevel='WARNING'):
        BaseLoggerClass.__init__(self, name, logLevel)

        self.name = str(name)

    #_______________________________________
    def Process(self, graphHolder):
        self.logger.debug('Execute {}'.format(self.name))
        self.graphHolder = graphHolder

        self.graphHolder.name = self.graphHolder.Options('name')
        self.graphHolder.type = self.graphHolder.Options('type')

        if self.graphHolder.type == 'TH1':
            self.TH1Processor()
        if self.graphHolder.type == 'TH1Diff':
            self.TH1DiffProcessor()
        if self.graphHolder.type == 'TH1Array':
            self.TH1ArrayProcessor()
        if self.graphHolder.type == 'Efficiency':
            self.EfficiencyProcessor()
        if self.graphHolder.type == 'TH2':
            self.TH2Processor()
        if self.graphHolder.type == 'TH2toProfile':
            self.TH2toProfileProcessor()
        if self.graphHolder.type == 'TreeToTH1':
            self.TreeToTH1Processor()
        if self.graphHolder.type == 'TreeToProfile':
            self.TreeToProfileProcessor()
        if self.graphHolder.type == 'TreeToEfficiency':
            self.TreeToEfficiencyProcessor(split_var_pass_all=False)
        if self.graphHolder.type == 'TreeToEfficiency2':
            self.TreeToEfficiencyProcessor(split_var_pass_all=True)
        if self.graphHolder.type == 'TreeToROC':
            self.TreeToROCProcessor()
        if self.graphHolder.type == 'Line':
            self.LineProcessor()
        if self.graphHolder.type == 'Point':
            self.PointProcessor()

        if self.graphHolder.Options('debug-break'):
            self.logger.info("Enter debug mode. Get latest graph via \"graph\" ")
            ROOT.gROOT.SetBatch(0)
            graph = self.graphHolder.graph
            c = ROOT.TCanvas()
            graph.Draw()
            c.Update()
            bp()

        return self.graphHolder

    #_______________________________________
    def TH1Processor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        path = self.graphHolder.Options('path')
        h = self.GetGraphFromFile(filePath, path)
        ROOT.SetOwnership(h, False)

        if (drawOpt["drawXerrors"]): 
            ex1 = ROOT.TExec("ex1", "gStyle->SetErrorX(0.5)")
            h.GetListOfFunctions().Add(ex1)

        h = self.Rebin(h, drawOpt)
        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetFillAttr(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = h

    #_______________________________________
    def TH1DiffProcessor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        path = self.graphHolder.Options('path')
        h = self.GetGraphFromFile(filePath, path)
        ROOT.SetOwnership(h, False)
        pathSubs = self.graphHolder.Options('pathSubs')
        hSubs = self.GetGraphFromFile(filePath, pathSubs)
        ROOT.SetOwnership(hSubs, False)

        h.Add(hSubs, -1)

        pathSubs2 = self.graphHolder.Options('pathSubs2')
        if pathSubs2:
            hSubs2 = self.GetGraphFromFile(filePath, pathSubs2)
            ROOT.SetOwnership(hSubs2, False)
            h.Add(hSubs2, -1)

        h = self.Rebin(h, drawOpt)
        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetFillAttr(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = h

        #_______________________________________
    def TH1ArrayProcessor(self):
        print "dkir TH1ArrayProcessor"
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        paths = self.graphHolder.Options('paths')
        h = None
        first_path = True
        for p in paths: 
            if first_path:
                first_path = False
                h = self.GetGraphFromFile(filePath, p)
                ROOT.SetOwnership(h, False)
            else:
                h.Add(self.GetGraphFromFile(filePath, p))


        h = self.Rebin(h, drawOpt)
        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetFillAttr(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = h

        if not drawOpt["apply-sys"] or len(self.graphHolder.GetSystematics()) == 0:
            return

        # Set systematics
        print "dkir systematics"
        for sys in self.graphHolder.GetSystematics():
            print "dkir sys: ", sys
            h_sys = None
            first_path = True
            for p in paths: 
                # bp() # modify path to point to systematics (e.g. SysFF_Transition_Btag__1up -> Systematics/)
                p_sys = "Systematics/"+p+"_"+sys
                if first_path:
                    first_path = False
                    h_sys = self.GetGraphFromFile(filePath, p_sys)
                    ROOT.SetOwnership(h_sys, False)
                else:
                    h_sys.Add(self.GetGraphFromFile(filePath, p_sys))

            h_sys = self.Rebin(h_sys, drawOpt)
            h_sys = self.Normalize(h_sys, drawOpt)
            h_sys = self.SetXRange(h_sys, drawOpt)
            # h_sys = self.SetYRange(h_sys, drawOpt)
            # h_sys = self.SetXYZTitle(h_sys, drawOpt)
            # h_sys = self.SetLineAttr(h_sys, drawOpt)
            # h_sys = self.SetMarkerAttr(h_sys, drawOpt)
            # h_sys = self.SetFillAttr(h_sys, drawOpt)
            self.graphHolder.sysgraphs[sys] = h_sys


    #_______________________________________
    def LineProcessor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        h = ROOT.TH1F(self.name, '', 1, drawOpt['xmin'], drawOpt['xmax'])
        h.SetDirectory(0)
        ROOT.SetOwnership(h, False)

        h.Fill(drawOpt['xmin'])
        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetFillAttr(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = h

    #_______________________________________
    def PointProcessor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        g = ROOT.TGraph()
        g.SetPoint(0, drawOpt['x'], drawOpt['y'])

        g = self.SetXRange(g, drawOpt)
        g = self.SetYRange(g, drawOpt)
        g = self.SetXYZTitle(g, drawOpt)
        g = self.SetLineAttr(g, drawOpt)
        g = self.SetMarkerAttr(g, drawOpt)
        g = self.SetFillAttr(g, drawOpt)

        # Set Graph
        self.graphHolder.graph = g

    #_______________________________________
    def getHistFromTree(self, tree, variable, cuts):
        drawOpt = self.graphHolder.Options('graph-opt')

        # prevent strings from iterating over each character
        if isinstance(variable, str):
            variable = [variable]

        # name for histogram is chosen randomly to prevent 
        # overwriting by later histograms 
        name = str(random.random())

        # there might be one or more variables
        for i in range(len(variable)):
            try:
                # there is either one list of cuts for all variables
                # or a list of lists if each variable has a unique set of cuts
                if isinstance(cuts[0], list):
                    cuts = cuts[i]
            except IndexError as e:
                self.logger.warning("list of cuts and list of variables do not match.")
                print "variables with {} element(s)".format(len(variable))
                print "cuts with {} element(s)".format(len(cuts))
                print e
                raise 

            # join cuts to one string, each surrounded by braces
            cutexpr = ['({})'.format(c) for c in cuts]
            cutexpr = "*".join(cutexpr)

            # get xmin or xmax from temporary hist if not specified in config
            xmin = drawOpt['xmin']
            xmax = drawOpt['xmax']
            ymin = drawOpt['ymin']
            ymax = drawOpt['ymax']

            if (None in [xmin, xmax]):
                tree.Draw(variable[i]+">>h_temp", cutexpr)
                h_temp = ROOT.gDirectory.Get("h_temp")

                if (xmin is None):
                    xmin = h_temp.GetXaxis().GetXmin()
                if (xmax is None):
                    xmax = h_temp.GetXaxis().GetXmax()

            if (i==0):
                if (':' in variable[i]): # 2D histogram
                    if drawOpt['bins'] is None:
                        h = ROOT.TH2F(name, '', drawOpt['nbins'], xmin, xmax, drawOpt['nbins'], ymin, ymax)
                    else: 
                        bins = array('f', drawOpt['bins'])
                        # h = ROOT.TH2F(name, '', len(bins)-1, bins, drawOpt['nbins'], ymin, ymax)
                        # work around because ROOT can not create TH2 with only variable x bins
                        from numpy import arange
                        ybins = arange(ymin, ymax+(ymax-ymin)/drawOpt['nbins'], (ymax-ymin)/drawOpt['nbins'])
                        ybins = array('f', ybins)
                        h = ROOT.TH2F(name, '', len(bins)-1, bins, len(ybins)-1, ybins)
                else: # 1D histogram
                    if drawOpt['bins'] is None:
                        h = ROOT.TH1F(name, '', drawOpt['nbins'], xmin, xmax)
                    else: 
                        bins = array('f', drawOpt['bins'])
                        h = ROOT.TH1F(name, '', len(bins)-1, bins)

            varexpr = "{}>>+{}".format(variable[i], name)

            # draw the variable (if i>0 the new histogram is added to the older one)
            tree.Draw(varexpr, cutexpr, drawOpt['draw-opt'].replace('SAME', ''))


        # return histogram from root directory
        return ROOT.gDirectory.Get(name)

        
    #_______________________________________
    def getSampleWeight(self, f, datasets):
        import re
        dsid = int(re.search("\.([0-9]{6})\.", f).group(1))
        if not dsid:
            self.logger.error("could not find dataset ID in file name {}".format(f))
            return 1
        if not dsid in datasets.keys():
            self.logger.error("dataset ID {} for file {} could not be found in dataset file".format(dsid, f))
            return 1

        dataset = datasets[dsid]
        file = ROOT.TFile(f)
        if (file.IsZombie()):
            self.logger.error('could not open weights file {}'.format(f))
            raise ValueError(f, 'is zombie.')

        if (dsid in range(361020, 361033)+range(361000, 361012)): # MC di-jet
            # number of initial events
            n_events = file.Get('Nominal/cutflow_DxAOD').GetBinContent(1)
        else:
            # weighted number of initial events
            n_events = file.Get('Nominal/cutflow_DxAOD').GetBinContent(2)
        file.Close()
        return (dataset['xsec'] * dataset['feff'] * dataset['kfac']) / n_events


    #_______________________________________
    def TreeToTH1Processor(self):
        opt = self.graphHolder.Options
        drawOpt = opt('graph-opt')

        input_file_names = self.GetInputFileNames('input-files')

        # if there is no cross section scaling, combine all files in one chain
        # in case of x-sec scaling get a weight for each file and combine them afterwards 
        if not drawOpt['xsec-scale-file']:
            if (len(input_file_names) == 1):
                chain  = ROOT.TTree()
                ROOT.SetOwnership(chain, False)
                file = ROOT.TFile(input_file_names[0])
                ROOT.SetOwnership(file, False)
                chain = file.Get(opt('path'))

            else: 
                chain = ROOT.TChain()
                for f in input_file_names:
                    chain.Add(f+"/"+opt("path"))
                ROOT.SetOwnership(chain, False)

            h = self.getHistFromTree(chain, opt("variable"), opt("cuts"))
        else:
            from YAMLHandler import YAMLHandler
            yH = YAMLHandler()
            datasets = yH.ReadYAML(drawOpt['xsec-scale-file'])

            first_file = True
            for f in input_file_names:
                tree = ROOT.TChain()
                tree.Add(f+"/"+opt("path"))
                ROOT.SetOwnership(tree, False)

                cuts = list(opt("cuts"))
                sample_weight = self.getSampleWeight(f, datasets)
                cuts.append(sample_weight)
                if first_file:
                    h = self.getHistFromTree(tree, opt("variable"), cuts).Clone()
                    first_file = False
                else:
                    h.Add(self.getHistFromTree(tree, opt("variable"), cuts))

        ROOT.SetOwnership(h, False)

        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.DevideByBinWidth(h, drawOpt)
        h = self.Rebin(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetFillAttr(h, drawOpt)
        h = self.SetColorPalette(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = h

    #_______________________________________
    def TreeToProfileProcessor(self):
        opt = self.graphHolder.Options
        drawOpt = opt('graph-opt')

        input_file_names = self.GetInputFileNames('input-files')

        chain = ROOT.TChain()
        for f in input_file_names:
            chain.Add(f+"/"+opt("path"))
        ROOT.SetOwnership(chain, False)

        h = self.getHistFromTree(chain, opt("variable"), opt("cuts"))
        if (h is None):
            self.logger.error('Histogram is empty')
        if (type(h) is not ROOT.TH2F): 
            self.logger.error('Histogram is expected to be of type TH2F')

        ROOT.SetOwnership(h, False)

        tg = self.GetTGraph(h, drawOpt)
        # bp()
        if (tg is None):
            self.logger.error('TGraph is empty')
            
        # tg = self.Normalize(tg, drawOpt)
        tg = self.SetXRange(tg, drawOpt)
        tg = self.SetYRange(tg, drawOpt)
        tg = self.SetXYZTitle(tg, drawOpt)
        tg = self.SetLineAttr(tg, drawOpt)
        tg = self.SetMarkerAttr(tg, drawOpt)
        # tg = self.SetFillAttr(h, drawOpt)

        # Set Graph
        self.graphHolder.graph = tg

    #_______________________________________
    def TreeToEfficiencyProcessor(self, split_var_pass_all=False):
        opt = self.graphHolder.Options
        drawOpt = opt('graph-opt')

        variable_all = ''
        variable_pas = ''

        if (split_var_pass_all):
            variable_pas = opt('variable_pass')
            variable_all = opt('variable_all')
        else:
            variable_pas = opt('variable')
            variable_all = opt('variable')

        cuts_all = opt("cuts_all")
        cuts_pas = opt("cuts_pass")

        chain = ROOT.TChain()
        for f in self.GetInputFileNames('input-files'):
            chain.Add(f+"/"+opt("path"))
        ROOT.SetOwnership(chain, False)

        h_pas = self.getHistFromTree(chain, variable_pas, cuts_pas)
        h_all = self.getHistFromTree(chain, variable_all, cuts_all)

        eff = ROOT.TGraphAsymmErrors(h_pas, h_all)
        
        eff = self.SetXRange(eff, drawOpt)
        eff = self.SetYRange(eff, drawOpt)
        eff = self.SetLineAttr(eff, drawOpt)
        eff = self.SetMarkerAttr(eff, drawOpt)
        eff = self.SetXYZTitle(eff, drawOpt)
        if not drawOpt["drawXerrors"]:
            for i in range(0, eff.GetN()):
                eff.SetPointEXhigh(i, 0)
                eff.SetPointEXlow(i, 0)

        self.graphHolder.graph = eff

    #_______________________________________
    def TreeToROCProcessor(self):
        opt = self.graphHolder.Options
        drawOpt = opt('graph-opt')
        nbins = drawOpt['nbins']

        variable_sig = opt('variable-signal')
        variable_bkg = opt('variable-background')

        cuts_sig = opt("cuts-signal")
        cuts_bkg = opt("cuts-background")
        cuts_sig = ['({})'.format(c) for c in cuts_sig]
        cuts_bkg = ['({})'.format(c) for c in cuts_bkg]
        cuts_sig = "*".join(cuts_sig)
        cuts_bkg = "*".join(cuts_bkg)

        file_paths_sig = GetInputFileNames("input-files-signal")
        file_paths_bkg = GetInputFileNames("input-files-background")

        tree_name_sig = opt("path-signal")
        tree_name_bkg = opt("path-background")

        chain_sig = ROOT.TChain()
        for f in file_paths_sig:
            chain_sig.Add(f+"/"+tree_name_sig)
        ROOT.SetOwnership(chain_sig, False)

        chain_bkg = ROOT.TChain()
        for f in file_paths_bkg:
            chain_bkg.Add(f+"/"+tree_name_bkg)
        ROOT.SetOwnership(chain_bkg, False)

        chain_sig.Draw(variable_sig+">>h_sig({},0,1)".format(nbins), cuts_sig)
        h_sig = ROOT.gDirectory.Get("h_sig")

        chain_bkg.Draw(variable_bkg+">>h_bkg({},0,1)".format(nbins), cuts_bkg)
        h_bkg = ROOT.gDirectory.Get("h_bkg")

        h_sig.Scale(1/h_sig.Integral())
        h_bkg.Scale(1/h_bkg.Integral())

        roc = ROOT.TGraph(nbins)
        for i in range(1, nbins+1):
            eff_sig = h_sig.Integral(i, nbins)
            eff_bkg = h_bkg.Integral(i, nbins)
            if (eff_bkg != 0):
                roc.SetPoint(i-1, eff_sig, 1/eff_bkg)

        ROOT.SetOwnership(roc, False)
        
        roc = self.SetXRange(roc, drawOpt)
        roc = self.SetYRange(roc, drawOpt)
        roc = self.SetLineAttr(roc, drawOpt)
        roc = self.SetMarkerAttr(roc, drawOpt)
        roc = self.SetXYZTitle(roc, drawOpt)

        self.graphHolder.graph = roc

    #_______________________________________
    def EfficiencyProcessor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        path_all = self.graphHolder.Options('path_all')
        path_pass = self.graphHolder.Options('path_pass')

        h_all = self.GetGraphFromFile(filePath, path_all)
        h_pass = self.GetGraphFromFile(filePath, path_pass)

        h_all = self.Rebin(h_all, drawOpt)
        h_pass = self.Rebin(h_pass, drawOpt)

        eff = ROOT.TGraphAsymmErrors(h_pass, h_all)

        eff = self.SetXRange(eff, drawOpt)
        eff = self.SetYRange(eff, drawOpt)
        eff = self.SetLineAttr(eff, drawOpt)
        eff = self.SetMarkerAttr(eff, drawOpt)
        eff = self.SetXYZTitle(eff, drawOpt)

        self.graphHolder.graph = eff

    #_______________________________________
    def TH2Processor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        path = self.graphHolder.Options('path')
        h = self.GetGraphFromFile(filePath, path)
        ROOT.SetOwnership(h, False)

        h = self.Normalize(h, drawOpt)
        h = self.SetXRange(h, drawOpt)
        h = self.SetYRange(h, drawOpt)
        h = self.SetXYZTitle(h, drawOpt)
        h = self.Rebin(h, drawOpt)
        h = self.SetLineAttr(h, drawOpt)
        h = self.SetMarkerAttr(h, drawOpt)
        h = self.SetColorPalette(h, drawOpt)
        h.GetXaxis().LabelsOption("v")
        # Set Graph
        self.graphHolder.graph = h

    #_______________________________________
    def TH2toProfileProcessor(self):
        drawOpt = self.graphHolder.Options('graph-opt')

        filePath = self.graphHolder.Options('input-file')
        path = self.graphHolder.Options('path')

        h2 = self.GetGraphFromFile(filePath, path)
        ROOT.SetOwnership(h2, False)
        
        if drawOpt['rebin'] != 0:
            h2.RebinX(drawOpt['rebin'])

        tg = self.GetTGraph(h2, drawOpt)

        tg = self.Normalize(tg, drawOpt)
        tg = self.SetXRange(tg, drawOpt)
        tg = self.SetYRange(tg, drawOpt)
        tg = self.SetXYZTitle(tg, drawOpt)
        # tg = self.Rebin(tg, drawOpt)
        tg = self.SetLineAttr(tg, drawOpt)
        tg = self.SetMarkerAttr(tg, drawOpt)

        # Set Graph
        self.graphHolder.graph = tg

    #_______________________________________
    def GetGraphFromFile(self, filePath, path):
        f_in = ROOT.TFile(filePath)
        if (f_in.IsZombie()):
            self.logger.error('could not open input file {}'.format(filePath))
            raise ValueError(filePath, 'is zombie.')
        try:
            h = f_in.Get(path)
            h.SetDirectory(0)
            ROOT.SetOwnership(h, False)
        except AttributeError, e:
            self.logger.warning("could not get {} from file {}".format(path, filePath))
            raise e

        f_in.Close()

        return h

    #_______________________________________
    def Normalize(self, h, drawOpt):
        norm = drawOpt['normalize']
        scale = drawOpt['scale']
        if norm != 0:
            h.Scale(norm/h.Integral())
        if scale != 1:
            h.Scale(scale)


        return h

    #_______________________________________
    def SetXRange(self, h, drawOpt):
        xmin = drawOpt['xmin']
        xmax = drawOpt['xmax']
        xaxis = h.GetXaxis()

        if not xmin:
            xmin = xaxis.GetXmin()
        if not xmax:
            xmax = xaxis.GetXmax()

        if xmin < xaxis.GetXmin():
            self.logger.warning('chosen minimum x bin is smaller than the minimum x bin in input histogram ({})'.format(self.graphHolder.name))
        if xmax > xaxis.GetXmax():
            self.logger.warning('chosen maximum x bin is bigger than the maximum x bin in input histogram ({})'.format(self.graphHolder.name))

        h.GetXaxis().SetRangeUser(xmin, xmax)

        return h

    #_______________________________________
    def SetYRange(self, h, drawOpt):
        ymin = drawOpt['ymin']
        ymax = drawOpt['ymax']
        if type(h) in [ROOT.TGraph, ROOT.TGraphErrors, ROOT.TGraphAsymmErrors]:
            if ymin == None:
                ymin = h.GetHistogram().GetMinimum()
            if ymax == None:
                ymax = 1.8 * h.GetHistogram().GetMaximum()
        elif type(h) in [ROOT.TH2, ROOT.TH2F, ROOT.TH2D]:
            if ymin == None:
                ymin = h.GetYaxis().GetXmin()
            if ymax == None:
                ymax = h.GetYaxis().GetXmax()
        else:
            if ymin == None:
                ymin = h.GetMinimum()
            if ymax == None:
                ymax = 1.4 * (h.GetBinContent(h.GetMaximumBin()) + h.GetBinError(h.GetMaximumBin()))

        if (drawOpt['bin-labels']):
            for i in range(len(drawOpt['bin-labels'])):
                h.GetXaxis().SetBinLabel(i+1, drawOpt['bin-labels'][i])
            h.GetXaxis().LabelsOption('v')

        self.logger.debug("y axis... SetRangeUser({}, {})".format(ymin, ymax))
        h.GetYaxis().SetRangeUser(ymin, ymax)
        ROOT.TGaxis.SetMaxDigits(4)

        return h

    # _______________________________________
    def SetXYZTitle(self, h, drawOpt):
        if drawOpt['x-title']:
            xaxis = h.GetXaxis()
            xaxis.SetTitle(drawOpt['x-title'])
        if drawOpt['y-title']:
            yaxis = h.GetYaxis()
            yaxis.SetTitle(drawOpt['y-title'])
        if drawOpt['z-title']:
            zaxis = h.GetZaxis()
            zaxis.SetTitle(drawOpt['z-title'])

        return h

    # _______________________________________
    def DevideByBinWidth(self, h, drawOpt):
        if not drawOpt['devideByBinWidth']:
            return h

        for i in range(h.GetNbinsX()+1):
            bincontent = h.GetBinContent(i)
            binwidht = h.GetBinWidth(i)
            h.SetBinContent(i, bincontent/binwidht)

        ytitle = h.GetYaxis().GetTitle()
        h.GetYaxis().SetTitle(ytitle+' / bin')
        return h

    # _______________________________________
    def Rebin(self, h, drawOpt):
        if drawOpt['rebin'] != 0:
            h.Rebin(drawOpt['rebin'])
        elif drawOpt['bins'] is not None:
            bins = array('d', drawOpt['bins'])
            h = h.Rebin(len(bins)-1, h.GetName()+'rebin', bins)

        return h

    # _______________________________________
    def SetLineAttr(self, h, drawOpt):
        lColor = drawOpt['line-color']
        if lColor in DKColor:
            h.SetLineColor(DKColor[lColor])
        else:
            self.logger.warning("color '{}' not found in DKColor.".format(lColor))

        lWidth = drawOpt['line-width']
        h.SetLineWidth(lWidth)

        lStyle = drawOpt['line-style']
        h.SetLineStyle(lStyle)

        return h

    # _______________________________________
    def SetMarkerAttr(self, h, drawOpt):
        mColor = drawOpt['marker-color']
        if mColor in DKColor:
            h.SetMarkerColor(DKColor[mColor])
        else:
            self.logger.warning("color '{}' not found in DKColor.".format(mColor))

        mSize = drawOpt['marker-size']
        h.SetMarkerSize(mSize)

        mStyle = drawOpt['marker-style']
        h.SetMarkerStyle(mStyle)

        return h

    # _______________________________________
    def SetFillAttr(self, h, drawOpt):
        mColor = drawOpt['fill-color']
        if mColor in DKColor:
            h.SetFillColor(DKColor[mColor])
        else:
            self.logger.warning("color '{}' not found in DKColor.".format(mColor))

        mStyle = drawOpt['fill-style']
        h.SetFillStyle(mStyle)

        return h

    # _______________________________________
    def SetColorPalette(self, h, drawOpt):
        color = drawOpt['palette']
        try:
            n_colors, palette = DKColor[color]
        except TypeError:
            self.logger.warning('could not retrieve color palette for color: {}'.format(color))
            return h
        ROOT.gStyle.SetPalette(n_colors, palette)

        return h

    # _______________________________________
    def GetTGraph(self, h2, drawOpt):
        nx = h2.GetNbinsX()

        hists = []
        fits = []
        xs = []
        dxs = []
        # make gaussian fits
        for i in xrange(1,nx+1):
          hist = h2.ProjectionY("_py_{}".format(i), i, i).Clone()
          # hist.GetXaxis().SetRangeUser(-2*hist.GetRMS(), 2*hist.GetRMS())
          hist.GetXaxis().SetRangeUser(hist.GetMean()-2*hist.GetRMS(), hist.GetMean()+2*hist.GetRMS())
          x = h2.GetXaxis().GetBinCenter(i)
          dx = h2.GetXaxis().GetBinWidth(i)
          mean = hist.GetMean()
          rms = hist.GetRMS()
          hist.Fit("gaus", "MR", "", mean-rms, mean+rms)
          fit = hist.GetFunction("gaus")
          hists.append(hist)
          fits.append(fit)
          xs.append(x)
          dxs.append(dx)

        tg = ROOT.TGraphErrors(nx)

        for i in range(0, nx):
          try:
            if drawOpt['plot-sigma'] == False:
                tg.SetPoint(i, xs[i], fits[i].GetParameter(1))  # parameter 1 is mean value
                tg.SetPointError(i, dxs[i]/2., fits[i].GetParError(1))
            elif drawOpt['plot-sigma'] == True:
                tg.SetPoint(i, xs[i], fits[i].GetParameter(2))  # parameter 2 is sigma
                tg.SetPointError(i, dxs[i]/2., fits[i].GetParError(2))
            else:
                self.logger.warning("can not parse plot-sigma option: {}".format(drawOpt['plot-sigma']))
          except Exception:
            self.logger.warning("could not set point {}".format(i))
            continue

        ROOT.SetOwnership(tg, False)
        return tg

    #_______________________________________
    def GetInputFileNames(self, opt_string):
        opt = self.graphHolder.Options

        input_file_names = []
        for f in opt(opt_string):
            new_files = glob.glob(f)
            if new_files == []: 
                self.logger.warning("no files found with name {}".format(f))
            if len(new_files) > 1:
                self.logger.info("{} files found with name {}".format(len(new_files), f))
            input_file_names += new_files

        return input_file_names
