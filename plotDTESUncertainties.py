import ROOT
from EmbeddedIPython import breakpoint as bp
from AtlasStyle import AtlasStyle
from DKColor import DKColor

def plot_DTES(t, dtes_type, cuts_pt ):

    hists = [ROOT.TH1F('{}_{}_{}'.format(dtes_type, cuts[0]/1000, cuts[1]/1000), ';(reco p_{{T}} - true vis p_{{T}}) / reco p_{{T}};'.format(cuts[0]/1000, dtes_type, cuts[1]/1000), 100, -1, 1) for cuts in cuts_pt] 
    [ROOT.SetOwnership(h, False) for h in hists]

    cs = [ROOT.TCanvas('c_'+h.GetName()) for h in hists]
    fits = []

    if dtes_type == 'ditau':
        variable = '((ditau_pt)-(ditau_truth_pt))/(ditau_pt)'
    elif dtes_type == 'leading tau':
        variable = '((ditau_sjlead_pt)-(ditau_truth_lead_pt))/(ditau_sjlead_pt)'
    elif dtes_type == 'subleading tau':
        variable = '((ditau_sjsubl_pt)-(ditau_truth_subl_pt))/(ditau_sjsubl_pt)'
    else:
        print 'dtes type "{}" unknown. Return.'.format(dtes_type)
        return

    for i in range(len(cuts_pt)):
        if dtes_type == 'ditau':
            cut = '(ditau_truthHadHad==1)&&(ditau_sjlead_pt+ditau_sjsubl_pt>{})&&(ditau_sjlead_pt+ditau_sjsubl_pt<{})'.format(cuts_pt[i][0], cuts_pt[i][1])
        elif dtes_type == 'leading tau':
            cut = '(ditau_truthHadHad==1)&&(abs(ditau_sjlead_eta-ditau_truth_lead_eta)<0.15)&&(abs(ditau_sjlead_phi-ditau_truth_lead_phi)<0.15)&&(ditau_sjlead_pt>{})&&(ditau_sjlead_pt<{})'.format(cuts_pt[i][0], cuts_pt[i][1])
        elif dtes_type == 'subleading tau':
            cut = '(ditau_truthHadHad==1)&&(abs(ditau_sjsubl_eta-ditau_truth_subl_eta)<0.15)&&(abs(ditau_sjsubl_phi-ditau_truth_subl_phi)<0.15)&&(ditau_sjsubl_pt>{})&&(ditau_sjsubl_pt<{})'.format(cuts_pt[i][0], cuts_pt[i][1])
        else:
            print 'dtes type "{}" unknown. Return.'.format(dtes_type)
            return


        # plot and fit
        t.Project(hists[i].GetName(), variable, cut)
        cs[i].cd()
        hists[i].Draw()
        mean = hists[i].GetMean()
        rms = hists[i].GetRMS()
        hists[i].Fit("gaus", "MRQ", "", mean-rms, mean+rms)
        fit = hists[i].GetFunction("gaus")
        fit.SetLineColor(DKColor['Red'])
        fits.append(fit)

        # plot legend 
        legend = ROOT.TLegend(0.22,0.88,0.82,0.95)
        legend.AddEntry(None, '{} GeV < {} p_{{T}} < {} GeV'.format(cuts_pt[i][0]/1000, dtes_type, cuts_pt[i][1]/1000), "")
        legend.SetBorderSize(0)
        ROOT.SetOwnership(legend, False)
        legend.Draw()

        cs[i].Update()
        cs[i].Print('output/dtes_uncertainty/'+hists[i].GetName()+'.pdf')

    # print results
    print '==========================================='
    print dtes_type + ' energy scale relative uncertainty '
    for i in range(len(cuts_pt)):
        print '{} < pt < {}: mean: {} sigma: {}'.format(cuts_pt[i][0], cuts_pt[i][1], fits[i].GetParameter(1), fits[i].GetParameter(2))
    print '==========================================='

    # plot results
    c = ROOT.TCanvas('c_'+dtes_type)
    ROOT.SetOwnership(c, False)
    g = ROOT.TGraphAsymmErrors()
    ROOT.SetOwnership(g, False)
    for i in range(len(cuts_pt)):
        pt_low = cuts_pt[i][0] / 1000
        pt_high = cuts_pt[i][1] / 1000
        mean = (pt_low + (pt_high - pt_low) / 2) 

        # delta pt = gaus sigma
        # g.SetPoint(i, mean, fits[i].GetParameter(2))
        # g.SetPointError(i, mean-pt_low, pt_high-mean, fits[i].GetParError(2), fits[i].GetParError(2) )

        # delta pt = absolute gaus mean
        g.SetPoint(i, mean, abs(fits[i].GetParameter(1)))
        g.SetPointError(i, mean-pt_low, pt_high-mean, fits[i].GetParError(1), fits[i].GetParError(1) )

    g.SetLineWidth(2)
    g.GetXaxis().SetTitle(dtes_type+' p_{T}')
    g.GetYaxis().SetTitle('#Delta p_{T} / p_{T} ')
    g.Draw('AP')

    c.Update()
    c.Print('output/dtes_uncertainty/'+dtes_type+'.pdf')


# ==================================
# main
# ==================================

style = AtlasStyle()
style.SetStyle()
style.SetPadTopMargin(0.14)

# f = ROOT.TFile('/ZIH.fast/users/kirchmeier/NTuples9/output_303367.DAOD_HIGG4D3_20170329_14-11-14/hist-user.dkirchme.mc15_13TeV.303367.DAOD_HIGG4D3.e4438_s2608_r6869_v01_EXT0.root')
# f = ROOT.TFile('/ZIH.fast/users/kirchmeier/NTuples9/output_425100.DAOD_HIGG4D3_20170329_14-28-08/hist-user.dkirchme.mc15_13TeV.425100.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root')
# t = f.Get('Nominal/BaselineSelection_tree_APPLY_DITAU_BASESELECTION(kDiTaus,0)')

file_names = [
    '/ZIH.fast/users/kirchmeier/NTuples9/output_425100.DAOD_HIGG4D3_20170329_14-28-08/hist-user.dkirchme.mc15_13TeV.425100.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root',
    '/ZIH.fast/users/kirchmeier/NTuples9/output_425101.DAOD_HIGG4D3_20170329_14-33-27/hist-user.dkirchme.mc15_13TeV.425101.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root',
    '/ZIH.fast/users/kirchmeier/NTuples9/output_425102.DAOD_HIGG4D3_20170329_14-38-13/hist-user.dkirchme.mc15_13TeV.425102.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root',
    '/ZIH.fast/users/kirchmeier/NTuples9/output_425103.DAOD_HIGG4D3_20170329_14-42-39/hist-user.dkirchme.mc15_13TeV.425103.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_EXT0.root',
    '/ZIH.fast/users/kirchmeier/NTuples9/output_425104.DAOD_HIGG4D3_20170329_14-46-48/hist-user.dkirchme.mc15_13TeV.425104.DAOD_HIGG4D3.e5485_s2726_r7772_r7676_v3_EXT0.root'
]

t = ROOT.TChain()
for f in file_names:
    t.Add(f+"/Nominal/BaselineSelection_tree_APPLY_DITAU_BASESELECTION(kDiTaus,0)")


cuts_pt = [[0, 300000], [300000, 500000], [500000, 700000], [700000, 1000000], [1000000, 2000000]]

## DiTau Energy Scale
plot_DTES(t, 'ditau', cuts_pt)

## Leading Tau Energy Scale
plot_DTES(t, 'leading tau', cuts_pt)

## Subleading Tau Energy Scale
cuts_pt_subl = [[0, 40000], [40000, 80000], [80000, 140000], [140000, 200000], [200000, 400000]]
plot_DTES(t, 'subleading tau', cuts_pt_subl)


