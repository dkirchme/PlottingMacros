#!/usr/bin/python
import ROOT

import sys
sys.argv.append( '-b-' )
ROOT.gROOT.SetBatch(True) 


import os
import time
import math

import glob
import itertools
from collections import OrderedDict
from EmbeddedIPython import breakpoint as bp
from DKColor import DKColor

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

  
def main():
    start_time = time.time()
    systematics = ['NOMINAL', 'ALT_GEO', 'IBL_30', 'PP0_50', 'QGSP_BIC']
    # systematics = ['NOMINAL']
    # systematics = ['NOMINAL', 'ALT_GEO']
    # systematics = ['NOMINAL', 'ALT_GEO', 'IBL_30']

    deltaR_regions = [
        ('deltaR-02-10', ['deltaR>0.2', 'deltaR<1.0']),
    ]
    leadPt_regions = [
        ('leadPt-00-200', ['lead_pt>0', 'lead_pt<200*1000']),
        ('leadPt-200-xx', ['lead_pt>200*1000'])
    ]
    sublPt_regions = [
        ('sublPt-00-20', ['subl_pt>0', 'subl_pt<20*1000']),
        ('sublPt-20-50', ['subl_pt>20*1000', 'subl_pt<50*1000']),
        ('sublPt-50-xx', ['subl_pt>50*1000'])
    ]

    working_points = OrderedDict()
    working_points['reco'] = '1'
    # working_points['veryloose'] = 'ditau_JetBDT>0.6'
    # working_points['loose'] = 'ditau_JetBDT>0.65'
    # working_points['medium'] = 'ditau_JetBDT>0.72'
    # working_points['tight'] = 'ditau_JetBDT>0.77'

    truth_cut = 'ditau_truthHadHad>0.5'

    # constructs regions dictionary, like:
    # {'var1-w-x_var2-y-z': ['var1>w', 'var1<x', 'var2>y', 'var2<z']}
    regions = {'_'.join([reg[0] for reg in regs]) : sum([reg[1] for reg in regs], []) for regs in itertools.product(deltaR_regions, sublPt_regions, leadPt_regions)  }
    regions['deltaR-00-02'] = ['deltaR>0', 'deltaR<0.2']
    regions['deltaR-10-xx'] = ['deltaR>1.0']

    dic = OrderedDict()
    for s in systematics: dic[s] = {}

    dic['NOMINAL']['file_names'] = [ get_file_names(f) for f in [   
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3126_r9364_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3126_r9364_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['ALT_GEO']['file_names'] = [ get_file_names(f) for f in [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3155_r9476_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3155_r9476_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['IBL_30']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3156_r9477_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3156_r9477_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['PP0_50']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3157_r9478_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3157_r9478_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
    ]]
    dic['QGSP_BIC']['file_names'] = [ get_file_names(f) for f in  [
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e5485_s3169_r9552_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3169_r9552_r9315.ntuple_v0_9_3.test02_hist/user.dkirchme.*.root',
        '/ZIH.fast/users/kirchmeier/MC16NTuples9/user.dkirchme.*.e6072_s3169_r9552_r9315.ntuple_v0_9_3.test02_2_hist/user.dkirchme.*.root',
    ]]


    dic['NOMINAL']['color'] = DKColor['RB1']
    dic['ALT_GEO']['color'] = DKColor['RB2']
    dic['IBL_30']['color'] = DKColor['RB3']
    dic['PP0_50']['color'] = DKColor['RB5']
    dic['QGSP_BIC']['color'] = DKColor['RB6']


    sf_range = [0.7, 0.8, 0.9, 0.92, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 0.995, 0.996, 0.997, 0.998, 0.999, 0.9995, 0.9998, 1.000, 1.0002, 1.0005, 1.001, 1.002, 1.003, 1.004, 1.005, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.08, 1.1, 1.2, 1.3]
    # ====================================
    # build systematics histograms
    # ====================================
    for (syst_name,syst) in dic.iteritems():
        # flatten file name lists
        syst['file_names'] = sum(syst['file_names'], [])

        # add all files to one tree
        syst['tree'] = ROOT.TChain()
        for f in syst['file_names']:
            syst['tree'].Add(f+'/Nominal/BaselineSelection_tree_APPLY_DITAU_BASESELECTION(kDiTaus,0)')

        # get histograms and efficiencies
        for (region_name, region) in regions.iteritems():

            for (wp_name, wp_cut) in working_points.iteritems():
                print syst_name, wp_name, region_name
                cuts = ['ditau_truth_'+ cut for cut in region]
                cuts.append(wp_cut)
                cuts.append(truth_cut)

                syst['hist_'+wp_name+region_name] = get_hist_from_tree(wp_name+syst_name+'_hist_'+region_name, syst['tree'], 'ditau_pt*0.001', cuts, 150, 0, 1500)
                if syst_name == 'NOMINAL':
                    for sf in sf_range:
                        syst['hist_{}_'.format(sf)+wp_name+region_name] = get_hist_from_tree(wp_name+syst_name+'_hist_{}_'.format(sf)+region_name, syst['tree'], 'ditau_pt*{}*0.001'.format(sf), cuts, 150, 0, 1500)





    # =================
    # make plots 
    # =================
    cs = []
    for reg_name in regions.keys():
        for wp_name in working_points.keys():
            print wp_name, reg_name
            cs.append(ROOT.TCanvas(wp_name+reg_name, wp_name+reg_name))
            cs[-1].cd()
            for sys_name in dic.keys():
                dic[sys_name]['hist_'+wp_name+reg_name].SetLineColor(dic[sys_name]['color'])
                dic[sys_name]['hist_'+wp_name+reg_name].Draw('same')

            cs.append(ROOT.TCanvas('chi2_'+wp_name+reg_name, 'chi2_'+wp_name+reg_name))
            cs[-1].cd()

            chi2 = ROOT.Double()
            first = True
            for sys_name in dic.keys():
                dic[sys_name]['chi2_'+wp_name+reg_name] = ROOT.TGraph(len(sf_range))
                dic[sys_name]['chi2values_'+wp_name+reg_name] = []
                # loop scale factors
                i = 0
                for sf in sf_range:
                    # get chi2 value
                    chi2 = dic[sys_name]['hist_'+wp_name+reg_name].Chi2Test(dic['NOMINAL']['hist_{}_'.format(sf)+wp_name+reg_name], 'chi2')
                    # add new chi2 point in graph
                    dic[sys_name]['chi2_'+wp_name+reg_name].SetPoint(i, sf, chi2)
                    dic[sys_name]['chi2values_'+wp_name+reg_name].append((sf, chi2))
                    i+=1

                dic[sys_name]['chi2_'+wp_name+reg_name].SetLineColor(dic[sys_name]['color'])
                dic[sys_name]['chi2_'+wp_name+reg_name].SetMarkerColor(dic[sys_name]['color'])

                if first: 
                    dic[sys_name]['chi2_'+wp_name+reg_name].Draw('alp')
                    dic[sys_name]['chi2_'+wp_name+reg_name].GetXaxis().SetTitle('scale factor')
                    dic[sys_name]['chi2_'+wp_name+reg_name].GetYaxis().SetTitle('#chi^{2}')
                else: dic[sys_name]['chi2_'+wp_name+reg_name].Draw('plsame')

                first = False


    # ================
    # make legend
    # ================
    cs.append(ROOT.TCanvas('legend', 'legend'))
    cs[-1].cd()
    leg = ROOT.TLegend(0.1, 0.1, 0.9, 0.9)    
    for sys_name in dic.keys():
        leg.AddEntry(dic[sys_name]['chi2_'+wp_name+reg_name], sys_name, 'pl')
    leg.Draw()


    # =======================================
    # retrieve systematics from histograms
    # =======================================
    print ' '
    print ' '
    print '================================================='
    print '=               uncertainty values              ='
    print '================================================='
    x = ROOT.Double()
    for region_name in regions.keys():
        for wp_name in working_points.keys():
            print wp_name, region_name
            uncert_squared_total = 0
            for (sys_name, syst) in dic.iteritems():
                if syst_name is 'NOMINAL': continue
                chi2vals = syst['chi2values_'+wp_name+region_name]
                sf_min, chi2_min = min(chi2vals, key=lambda x: x[1])
                syst['uncert_'+wp_name+region_name] = abs(sf_min-1)

                uncert_squared_total += syst['uncert_'+wp_name+region_name]**2

            dic['NOMINAL']['uncert_'+wp_name+region_name] = math.sqrt(uncert_squared_total)

            print ('TOTAL %.6f' % (dic['NOMINAL']['uncert_'+wp_name+region_name]))
            print '================================================='
        
    print ' '
    print ' '

    # =======================================
    # plot uncertainty values
    # =======================================
    h_uncert = ROOT.TH2F('uncertainty', 'uncertainty', len(dic.keys()), 0, len(dic.keys()), len(regions), 0, len(regions))

    # set labels
    i = 1
    for syst in dic.keys(): 
        if syst is 'NOMINAL': 
            syst_name = 'TOTAL'
        else:
            syst_name = syst
        h_uncert.GetXaxis().SetBinLabel(i, syst_name)
        i+=1
    i = 1
    for reg_name in reversed(sorted(regions.keys())):
        h_uncert.GetYaxis().SetBinLabel(i, reg_name)
        i+=1

    # fill histogram
    for syst in dic.keys():
        if syst is 'NOMINAL': 
            syst_name = 'TOTAL'
        else:
            syst_name = syst
        for reg_name in regions.keys(): 
            h_uncert.Fill(syst_name, reg_name, dic[syst]['uncert_reco'+reg_name])


    # set color palette
    n_colors, palette = DKColor['WhiteToRed']
    ROOT.gStyle.SetPalette(n_colors, palette)
    ROOT.gStyle.SetPaintTextFormat("4.4f")
    
    # plot uncertainties
    cs.append(ROOT.TCanvas('uncertainty', 'uncertainty', 1500, 1000))
    cs[-1].cd()
    cs[-1].SetLeftMargin(0.4)
    h_uncert.Draw('COLTEXT')

    # create output file    
    f = ROOT.TFile('output/tessystematic_results/tessystematic.root', 'RECREATE')
    for c in cs:
        c.Print('output/tessystematic_results/'+c.GetName()+'.png')
        c.Write()

    print('===============================')
    print('processing time: %.2f s' % (time.time() - start_time))
    print('===============================')

    bp()

    f.Close()
    return


def get_file_names(s):
    files = glob.glob(s)
    if files == []: 
        print("WARNING: no files found with name {}".format(s))

    return files


def get_hist_from_tree(hist_name, tree, variable, cuts, nbins=1, xmin=0.5, xmax=1.5):
    cutexpr = ['({})'.format(c) for c in cuts]
    cutexpr = "*".join(cutexpr)
    h = ROOT.TH1F(hist_name, hist_name, nbins, xmin, xmax)

    tree.Project(hist_name, variable, cutexpr)

    n = h.GetEntries()
    if (n < 150 ):
        h.Rebin(15)
    elif (n < 1500):
        h.Rebin(10)
    elif (n < 5000):
        h.Rebin(5)
    elif (n < 15000):
        h.Rebin(3)
    elif (n < 50000):
        h.Rebin(2)

    return h


if __name__ == '__main__':
    main()
