#!/usr/bin/python

import ROOT
import os
import sys
from math import sqrt
from collections import OrderedDict
from array import array
from DKColor import getWhiteToBlue
from DKColor import DKRainbow
from DKColor import DKColor

ROOT.gROOT.SetBatch(1)

ROOT.gROOT.Macro( os.path.expanduser('~/.root/AtlasStyle.C'))
from ROOT import SetAtlasStyle
SetAtlasStyle()

# ROOT.gStyle.SetPaintTextFormat(".3f")
# n_colors, palette = getWhiteToBlue()
# ROOT.gStyle.SetPalette(n_colors, palette)

# ROOT.gStyle.SetEndErrorSize(3)
ROOT.gStyle.SetErrorX(0.5)
ROOT.gStyle.SetHatchesLineWidth(1)

# get lists of one and two sided systematics
def get_systematics(f, fake_factor):
    systematics = [k.GetName() for k in f.GetListOfKeys() if fake_factor in k.GetName() and "finebinned" not in k.GetName() and (fake_factor+"_") not in k.GetName() and fake_factor!=k.GetName() and "onebin" not in k.GetName()]
    systematics = [s[s.find(fake_factor)+len(fake_factor):] for s in systematics]

    systematics_onesided = []
    systematics_twosided = []
    for s in systematics:
        # base = ""
        # if s.endswith("__1down"): base = s[:-7]
        # if s.endswith("__1up"): base = s[:-5]
        base = s.split("__")[0]
        if (base+"__1up" in systematics) and (base+"__1down" in systematics):
            if base not in systematics_twosided:
                systematics_twosided.append(base)
        else:
            systematics_onesided.append(base)
    return (systematics_onesided, systematics_twosided)

# 
def plot_syst_details(title, h_nom, title_nom, h_sys_up, title_sys_up, h_sys_dw = None, title_sys_dw = ""):
    c = ROOT.TCanvas("syst_details", "", 800, 600)
    if "2tag" in title:
        h_sys_up.GetYaxis().SetRangeUser(0., 0.018)
    else:
        h_sys_up.GetYaxis().SetRangeUser(0., 0.012)
    h_sys_up.GetXaxis().SetTitle("Di-tau p_{T} [GeV]")
    h_sys_up.GetYaxis().SetTitle("Fake factor")
    h_sys_up.SetLineColor(DKColor['Blue2'])
    h_sys_up.SetMarkerColor(DKColor['Blue2'])
    h_sys_up.SetFillColor(DKColor['Blue1'])
    h_sys_up.SetFillStyle(3245)
    h_sys_up.Draw("SAMEE2")

    if h_sys_dw: 
        h_sys_dw.SetLineColor(DKColor['Red2'])
        h_sys_dw.SetMarkerColor(DKColor['Red2'])
        h_sys_dw.SetFillColor(DKColor['Red'])
        h_sys_dw.SetFillStyle(3254)
        h_sys_dw.Draw('SAMEE2')
        h_sys_dw.Draw('SAME')

    h_sys_up.Draw("SAME")

    leg_x1 = 0.37 
    leg_y1 = 0.21
    leg_x2 = 0.92
    leg_y2 = 0.35
    # leg = ROOT.TLegend(0.37 , 0.21, 0.92, 0.46)
    # leg = ROOT.TLegend(0.37 , 0.21, 0.92, 0.35)
    # if 'Btag' in title:
        # leg = ROOT.TLegend(0.37 , 0.71, 0.92, 0.91)
        # leg = ROOT.TLegend(0.37 , 0.82, 0.92, 0.91)
    leg_y1 = 0.82
    leg_y2 = 0.91
    leg_y1 = leg_y2 - 2 * 0.04
    if h_sys_dw:
        leg_y1 = leg_y2 - 3 * 0.04

    leg = ROOT.TLegend(leg_x1, leg_y1, leg_x2, leg_y2)


    if h_nom:
        h_nom.Draw('SAME')
        leg.AddEntry(h_nom, title_nom)

    # y1 = y2 - leg_opt['dy']*n_leg_entries

    leg.AddEntry(h_sys_up, title_sys_up)

    if h_sys_dw:
        leg.AddEntry(h_sys_dw, title_sys_dw)
    leg.Draw('NB')

    # atlas_label = get_atlas_label()
    # atlas_label.Draw()
    get_atlas_label()
    c.SaveAs("output/fakeSystematics/systematic_{}.pdf".format(title))
    # from pdb import set_trace as bp
    # bp()

def get_atlas_label(text="#bf{#it{ATLAS}} Internal", x=0.215, y=0.88, dy=0.055, size=0.035):

    if isinstance(text, str):
        text = [text]
        # y = [y]

    for i in range(len(text)): 
        box = ROOT.TLatex(x, y, text[i])
        box.SetNDC()
        y = y - dy
        box.SetTextSize(size)
        ROOT.SetOwnership(box, False)
        box.SetNDC()
        box.Draw()



#############################################
#############################################
# main
#############################################
#############################################
# f_path = "/home/s3608936/Code/DiHiggsAnalysisR21/FakeFactorCalc/FFqcd_boosted-v9_calcFF_Rv41.root"
# f_path = "/home/s3608936/Code/DiHiggsAnalysisR21/FakeFactorCalc/FFqcd_boosted.root"
f_path = "/scratch/ws/s3608936-DiHiggsAnalysis/FakeFactorCalc/FFqcd_boosted.root"
# f_path = "/scratch/ws/s3608936-DiHiggsAnalysis/FakeFactorCalc/FFqcd_boosted-v10_calcFF_Rv71.root"
# f_path = "/scratch/ws/s3608936-DiHiggsAnalysis/FakeFactorCalc/FFqcd_boosted-v10_calcFF_Rv72.root"
# f_path = "/scratch/ws/s3608936-DiHiggsAnalysis/FakeFactorCalc/FFqcd_boosted-v10_calcFF_Rv73.root"
fake_factor = "h_FF_Ptbinned_SS_0tag"
f = ROOT.TFile(f_path)

plot_all_systematics = False


if plot_all_systematics:
    systematics_onesided, systematics_twosided = get_systematics(f, fake_factor)
else: 
    # systematics_twosided = ["FATJET_Medium_JET_Comb_Modelling_Kin", "FATJET_Medium_JET_Comb_Baseline_Kin", "FATJET_Medium_JET_Comb_Tracking_Kin", "DiTauSF_Syst"]
    # systematics_twosided = ["FATJET_Medium_JET_Comb_Modelling_Kin", "FATJET_Medium_JET_Comb_Baseline_Kin", "DiTauSF_Syst"]
    systematics_twosided = ["FATJET_Medium_JET_Comb_Modelling_Kin", "FATJET_Medium_JET_Comb_Tracking_Kin", "DiTauSF_Syst"]
    # systematics_twosided = []
    systematics_onesided = []
systematics_0tagVs1tag = ["FF_Transition_Btag"]
systematics_SSvsOS = ["FF_Transition_Sign"]
# uncert_map: {sysname: [(up bin1, down bin1), (up bin2, down bin2), ...], sysname2: [...], ...}
uncert_dict = {sys: [] for sys in (systematics_onesided+systematics_twosided+systematics_0tagVs1tag+systematics_SSvsOS)}

#  nominal FF histogram
h_nom = f.Get(fake_factor)
h_nom.GetYaxis().SetRangeUser(0., 0.01)
h_nom.GetXaxis().SetTitle("Di-tau p_{T} [GeV]")
h_nom.GetYaxis().SetTitle("Fake factor")
N_bins = h_nom.GetNbinsX()


legs = []
#############################################
# two sided systematics
#############################################
for sys in systematics_twosided:
    h_sys_up = f.Get(fake_factor + sys + "__1up")
    h_sys_dw = f.Get(fake_factor + sys + "__1down")
    plot_syst_details(sys, h_nom, "Nominal fake factors", h_sys_up, sys+"__1up", h_sys_dw, sys+"__1down")

    for i in range(1, N_bins+1):
        uncert_up = h_sys_up.GetBinContent(i) - h_nom.GetBinContent(i)
        uncert_dw = h_sys_dw.GetBinContent(i) - h_nom.GetBinContent(i)

        if (uncert_up*uncert_dw > 0):
            print "WARNING: systematics shift in same direction... mirror larger uncertainty (sys: {}, bin: {})".format(sys, i)
            uncert_max = max(abs(uncert_up), abs(uncert_dw))
            uncert_up = uncert_max
            uncert_dw = uncert_max

        uncerts = ( abs(max(uncert_up, uncert_dw)), abs(min(uncert_up, uncert_dw)) ) # (up variation , down variation)
        uncert_dict[sys].append( uncerts )

#############################################
# one sided systematics
#############################################
for sys in systematics_onesided:
    h_sys_up = f.Get(fake_factor + sys + "__1up")
    plot_syst_details(sys, h_nom, "Nominal fake factors", h_sys_up, sys)

    for i in range(1, N_bins+1):
        uncert_up = h_sys_up.GetBinContent(i) - h_nom.GetBinContent(i)

        uncert_dict[sys].append( (abs(uncert_up), abs(uncert_up)) )

#############################################
# systematics from 0-tag vs 1-tag comparison
#############################################
for sys in systematics_0tagVs1tag:
    # h_nom_onebin = f.Get(fake_factor + "_onebin")
    if (fake_factor[-5:] == "_0tag"):
        h_0tag = f.Get(fake_factor[:-5] + "_0tag_onebin")
        h_1tag = f.Get(fake_factor[:-5] + "_1tag_onebin")
        h_2tag = f.Get(fake_factor[:-5] + "_2tag_onebin")
    else: 
        h_0tag = f.Get(fake_factor + "_0tag_onebin")
        h_1tag = f.Get(fake_factor + "_1tag_onebin")


    plot_syst_details(sys+"_with2tag", h_0tag, "SS, 0 b-tag", h_1tag, "SS, 1 b-tag", h_2tag, "SS, 2 b-tag" )
    # plot_syst_details(sys, None, "", h_0tag, "Fake factor, 0-tag region", h_1tag, "Fake factor, 1-tag region")
    plot_syst_details(sys, h_0tag, "SS, 0 b-tag", h_1tag, "SS, 1 b-tag")

    uncert = abs(h_0tag.GetBinContent(1) - h_1tag.GetBinContent(1))
    for i in range(1, N_bins+1):
        uncert_dict[sys].append( (uncert, uncert) )

#############################################
# systematics from SS vs OS comparison
#############################################
for sys in systematics_SSvsOS:
    # dkir compare only in 0-tag or b-tag inclusive? 
    # h_SS = f.Get(fake_factor + "_0tag")
    # h_OS = f.Get(fake_factor[:-2] + "OS_0tag")
    h_SS = f.Get(fake_factor)
    h_OS = f.Get(fake_factor.replace("SS", "OS"))
    # plot_syst_details(sys, None, "", h_SS, "Fake factors, SS region", h_OS, "Fake factors, OS region")
    plot_syst_details(sys, h_SS, "SS, 0 b-tag", h_OS, "OS, 0 b-tag")

    for i in range(1, N_bins+1):
        uncert = abs(h_SS.GetBinContent(i) - h_OS.GetBinContent(i))
        uncert_dict[sys].append( (uncert, uncert) )


# print results
# for k in uncert_dict: print "\n", k, uncert_dict[k]


#############################################
# draw results
#############################################
# atlas_label = get_atlas_label(["#bf{#it{ATLAS}} Internal", "SS, 0 b-tag"])
c = ROOT.TCanvas("results", "", 800, 600)
h_nom.Draw()
# atlas_label.Draw()
get_atlas_label(["#bf{#it{ATLAS}} Internal", "13 TeV, 139 fb^{-1}", "FF SS CR"])

y_nom = array('d', [h_nom.GetBinContent(i) for i in range(1, N_bins+1)])
x_nom = array('d', [h_nom.GetBinCenter(i) for i in range(1, N_bins+1)])

leg = ROOT.TLegend(0.37 , 0.21, 0.92, 0.46)
leg.AddEntry(h_nom, "Fake factors with stat. uncertainties")
leg.Draw('NB')

# sort uncertainties
uncert_dict = OrderedDict(sorted(uncert_dict.items(), key=lambda t: t[1][0][0], reverse=True))
g_list = []



i = 0
for sys in uncert_dict.keys():

    # cs.append(ROOT.TCanvas())
    uncert_up = array('d', [t[0] for t in uncert_dict[sys]])
    uncert_dw = array('d', [t[1] for t in uncert_dict[sys]])
    zero = array('d', [0]*N_bins)
    x_nom = array('d', [x+10 for x in x_nom])

    # TGraphAsymmErrors (Int_t n, const Float_t *x, const Float_t *y, const Float_t *exl=0, const Float_t *exh=0, const Float_t *eyl=0, const Float_t *eyh=0)
    g_list.append(ROOT.TGraphAsymmErrors(N_bins, x_nom, y_nom, zero, zero, uncert_dw, uncert_up))
    g_list[-1].SetLineWidth(3)
    g_list[-1].SetMarkerSize(0)
    g_list[-1].SetMarkerColor(DKRainbow[i])
    g_list[-1].SetLineColor(DKRainbow[i])
    leg.AddEntry(g_list[-1], sys)

    g_list[-1].Draw("PSAME")
    i = (i+1)%10

c.SaveAs("output/fakeSystematics/systematics.pdf")

cnom = ROOT.TCanvas("nominal", "", 800, 600)
h_nom.Draw()
# atlas_label.Draw()
get_atlas_label(["#bf{#it{ATLAS}} Internal", "13 TeV, 139 fb^{-1}", "FF SS CR"])
cnom.SaveAs("output/fakeSystematics/fakefactors.pdf")

